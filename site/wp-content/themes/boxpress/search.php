<?php
/**
 * The template for displaying search results pages.
 *
 * @package BoxPress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--search.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap wrap--limited">

      <?php if ( have_posts() ) : ?>

        <header class="page-header">
          <h1 class="page-title search-title"><?php printf( __( 'Search Results for: %s', 'boxpress' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        </header>

        <?php while ( have_posts() ) : the_post(); ?>

          <?php get_template_part( 'template-parts/content', 'search' ); ?>

        <?php endwhile; ?>

        <?php the_posts_navigation(); ?>

      <?php else : ?>
        <?php get_template_part( 'template-parts/content', 'none' ); ?>
      <?php endif; ?>

    </div>
  </section>
<?php get_footer(); ?>
