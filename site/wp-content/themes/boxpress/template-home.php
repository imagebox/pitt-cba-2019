<?php
/**
 * Template Name: Homepage
 */


get_header(); ?>

  <article class="homepage">
    <?php get_template_part('template-parts/homepage-hero'); ?>

    <?php if( have_rows('home_page') ): ?>

    <section class="home-section button-block-media-section-why">
      <div class="wrap">
        <?php while ( have_rows('home_page') ) : the_row(); ?>
        <?php if( have_rows('why_pitt_business') ): ?>
       	<?php while ( have_rows('why_pitt_business') ) : the_row(); ?>


          <div class="button-block-media ">


              <a class="video-button" href="<?php the_sub_field('video_link');?>">
                <?php $background_image_block_why_media = get_sub_field('background_image_block_why_media'); ?>

                <?php if ( $background_image_block_why_media ) :
                    $background_image_block_why_media_url  = $background_image_block_why_media['sizes'][ 'background_image_block_why_media' ];
                    $background_image_block_why_media_w    = $background_image_block_why_media['sizes'][ 'background_image_block_why_media' . '-width' ];
                    $background_image_block_why_media_h    = $background_image_block_why_media['sizes'][ 'background_image_block_why_media' . '-height' ];
                  ?>


                <div class="media-block">

                <img class="play-button" src="<?php bloginfo('template_directory'); ?>/assets/img/play.png" />
                  <img class="bkg-image" src="<?php echo $background_image_block_why_media_url; ?>"
                    width="<?php echo  $background_image_block_why_media_w; ?>"
                    height="<?php echo $background_image_block_why_media_h; ?>"
                    alt="">
                </div>


              <?php endif; ?>
              </a>


          <div class="button-block-copy button-block-copy-why">
            <div class="button-block-header">
              <h2><?php the_sub_field('button_block_title_why_media'); ?></h2>
            </div>
            <div class="button-block-body">
                <p><?php the_sub_field('button_block_body_why_media'); ?></p>

              <?php

              $button_block_link_why_media= get_sub_field('button_block_link_why_media');

              if( $button_block_link_why_media ): ?>

              	<a class="button" href="<?php echo $button_block_link_why_media['url']; ?>" target="<?php echo $button_block_link_why_media['target']; ?>"><?php echo $button_block_link_why_media['title']; ?></a>

              <?php endif; ?>
            </div>



          </div>
        </div>

        </div>
        </div>
          <!-- copy -->

        <?php endwhile; ?>
        <?php endif; ?>
      <?php endwhile; ?>

    </section>
  <?php endif; ?>
  <!-- end why pitt business -->



  <?php if( have_rows('home_page') ): ?>
  <section class="graphic-block-section home-graphic-block button-block-media-section-one">
      <div class="wrap">

            <div class="graphic-block">
              <?php while ( have_rows('home_page') ) : the_row(); ?>
              <?php if( have_rows('pitt_statistics') ): ?>
              <?php while ( have_rows('pitt_statistics') ) : the_row(); ?>
              <?php if ( have_rows( 'graphic_block' )) : ?>

                <?php while ( have_rows( 'graphic_block' )) : the_row();
                    $icon_text = get_sub_field('stat_icon_text');
                    $image = get_sub_field('stat_icon');
                  ?>

                <div class="graphic-block-content pitt-line-graphic-block">
                  <?php if ( $image ) : ?>
                    <img src="<?php echo $image; ?>" alt="">
                  <?php endif; ?>

                  <?php if ( ! empty($icon_text) ) : ?>
                        <h3><?php echo the_sub_field('stat_icon_text'); ?></h3>
                  <?php endif; ?>
                </div>

                  <?php endwhile; ?>
                <?php endif; ?>
              <?php endwhile; ?>
              <?php endif; ?>
            <?php endwhile; ?>
            </div>

        </div>
    </section>
    <?php endif; ?>
    <!-- end pitt graphic block -->




  <?php if( have_rows('home_page') ): ?>

  <section class="home-section button-block-media-section-about">
    <div class="wrap">
      <?php while ( have_rows('home_page') ) : the_row(); ?>
      <?php if( have_rows('about_pittsburgh') ): ?>
      <?php while ( have_rows('about_pittsburgh') ) : the_row(); ?>

      <div class="button-block-media">
        <div class="button-block-copy button-block-copy-about">
          <div class="button-block-header">
            <h2><?php the_sub_field('button_block_title_about_media'); ?></h2>
          </div>
          <div class="button-block-body">
            <p><?php the_sub_field('button_block_body_about_media'); ?></p>

          <?php

          $button_block_link_about_media = get_sub_field('button_block_link_about_media');

          if( $button_block_link_about_media ): ?>

          	<a class="button" href="<?php echo $button_block_link_about_media['url']; ?>" target="<?php echo $button_block_link_about_media['target']; ?>"><?php echo $button_block_link_about_media['title']; ?></a>

          <?php endif; ?>
          </div>
        </div>


          <?php $background_image_block_about_media = get_sub_field('background_image_block_about_media'); ?>

          <?php if ( $background_image_block_about_media ) :
              $background_image_block_about_media_url  = $background_image_block_about_media['sizes'][ 'background_image_block_about_media' ];
              $background_image_block_about_media_w    = $background_image_block_about_media['sizes'][ 'background_image_block_about_media' . '-width' ];
              $background_image_block_about_media_h    = $background_image_block_about_media['sizes'][ 'background_image_block_about_media' . '-height' ];
            ?>

            <img class="media-block bkg-image" src="<?php echo $background_image_block_about_media_url; ?>"
              width="<?php echo  $background_image_block_about_media_w; ?>"
              height="<?php echo $background_image_block_about_media_h; ?>"
              alt="">

        <?php endif; ?>

        <!-- media block -->
    </div>
        <!-- end media block -->
      </div>

      <?php endwhile; ?>
      <?php endif; ?>
    <?php endwhile; ?>

  </section>
<?php endif; ?>
<!-- end about pittsburgh -->




<?php if( have_rows('home_page') ): ?>

<section class="home-section button-block-media-section-apply">
    <div class="wrap">
      <?php while ( have_rows('home_page') ) : the_row(); ?>
      <?php if( have_rows('apply_pitt_business') ): ?>
      <?php while ( have_rows('apply_pitt_business') ) : the_row(); ?>


        <div class="apply-pitt-block-header">
          <h2><?php the_sub_field('apply_pitt_heading'); ?></h2>
        </div>

        <?php if ( have_rows( 'button_blocks' )) : ?>

          <div class="apply-pitt-block">
            <?php while ( have_rows( 'button_blocks' )) : the_row();
                $button_link = get_sub_field('button_url');
                $image = get_sub_field('media_image');
              ?>

              <?php if ( ! empty($button_link) ) : ?>
                <div class="apply-pitt-col bkg-image-container <?php echo get_sub_field('background_color'); ?>">
                  <a href="<?php echo $button_link; ?>">
                    <h6><?php echo the_sub_field('heading'); ?></h6>

                  <?php if ( $image ) : ?>
                    <img class="bkg-image" src="<?php echo $image['url']; ?>" alt="">
                  <?php endif; ?>


                    </a>
                </div>

              <?php endif; ?>


            <?php endwhile; ?>
          </div>
        <?php endif; ?>

    </div>
  <!-- end buttons-->

    <?php endwhile; ?>
    <?php endif; ?>
  <?php endwhile; ?>
</section>
<?php endif; ?>
<!-- end applying to pitt business -->


<?php if( have_rows('home_page') ): ?>

<section class="home-section button-block-media-section-admission">
    <div class="wrap">
      <?php while ( have_rows('home_page') ) : the_row(); ?>
      <?php if( have_rows('admission_requirements') ): ?>
      <?php while ( have_rows('admission_requirements') ) : the_row(); ?>


        <div class="admission_requirements_heading_block">
          <h2><?php the_sub_field('admission_requirements_headline'); ?></h2>
        </div>

        <?php if ( have_rows( 'media_blocks' )) : ?>

          <div class="admission_requirements_button_block">
            <?php while ( have_rows( 'media_blocks' )) : the_row();
                $button_link = get_sub_field('button_url');
                $admission_req_image  = get_sub_field( 'admission_req_image' );
              ?>
              <?php if ( ! empty($button_link) ) : ?>
                <div class="admission_requirements_col bkg-image-container <?php echo get_sub_field('background_color'); ?>">
                  <a href="<?php echo $button_link; ?>">
                    <h6><?php echo the_sub_field('heading'); ?></h6>
                  </a>

                  <?php if ( $admission_req_image ) :
                      $admission_req_image_url  = $admission_req_image['sizes'][ 'admission_req_image' ];
                      $admission_req_image_w    = $admission_req_image['sizes'][ 'admission_req_image' . '-width' ];
                      $admission_req_image_h    = $admission_req_image['sizes'][ 'admission_req_image' . '-height' ];
                    ?>
                      <img class="bkg-image" src="<?php echo $admission_req_image_url; ?>"
                        width="<?php echo  $admission_req_image_w; ?>"
                        height="<?php echo $admission_req_image_h; ?>"
                        alt="">
                  <?php endif; ?>

                </div>
              <?php endif; ?>


            <?php endwhile; ?>
          </div>
        <?php endif; ?>
    </div>
  <!-- end buttons-->

    <?php endwhile; ?>
    <?php endif; ?>
  <?php endwhile; ?>
</section>
<?php endif; ?>
<!-- end admission requirements -->



  <?php if( have_rows('home_page') ): ?>

  <section class="home-section button-block-media-section-career-conference">
    <div class="wrap">
      <?php while ( have_rows('home_page') ) : the_row();



       ?>
      <?php if( have_rows('career_conference') ): ?>
      <?php while ( have_rows('career_conference') ) : the_row(); ?>



      <div class="button-block-media">

        <div class="button-block-copy button-block-copy-career">
          <div class="button-block-header">
            <h2><?php the_sub_field('button_block_title_career_conference_media'); ?></h2>
          </div>
          <div class="button-block-body">
            <p><?php the_sub_field('button_block_body_career_conference_media'); ?></p>


<?php $button_block_link_career_conference_media = get_sub_field('button_block_link_career_conference_media'); ?>


        <?php  if( $button_block_link_career_conference_media ): ?>

        	<a class="button" href="<?php echo $button_block_link_career_conference_media['url']; ?>" target="<?php echo $button_block_link_career_conference_media['target']; ?>"><?php echo $button_block_link_career_conference_media['title']; ?></a>

        <?php endif; ?>
          </div>
        </div>


        <div class="media-block">
          <?php $background_image_block_career_conference_media = get_sub_field('background_image_block_career_conference_media'); ?>

          <?php if ( $background_image_block_career_conference_media ) :
              $background_image_block_career_conference_media_url  = $background_image_block_career_conference_media['sizes'][ 'background_image_block_career_conference_media' ];
              $background_image_block_career_conference_media_w    = $background_image_block_career_conference_media['sizes'][ 'background_image_block_career_conference_media' . '-width' ];
              $background_image_block_career_conference_media_h    = $background_image_block_career_conference_media['sizes'][ 'background_image_block_career_conference_media' . '-height' ];
            ?>

            <img class="bkg-image" src="<?php echo $background_image_block_career_conference_media_url; ?>"
              width="<?php echo  $background_image_block_career_conference_media_w; ?>"
              height="<?php echo $background_image_block_career_conference_media_h; ?>"
              alt="">

        <?php endif; ?>
  </div>


        <!-- photo-->
      </div>
    </div>
        <!-- copy -->

      <?php endwhile; ?>
      <?php endif; ?>
    <?php endwhile; ?>

  </section>
<?php endif; ?>
<!-- end why career conference -->


<?php if( have_rows('home_page') ): ?>

<section class="home-section button-block-media-section-five">
  <div class="wrap">
    <?php while ( have_rows('home_page') ) : the_row(); ?>
    <?php if( have_rows('pitt_business_instagram') ): ?>
    <?php while ( have_rows('pitt_business_instagram') ) : the_row(); ?>


        <div class="insta-heading-body">
          <img src="<?php the_sub_field('insta_icon'); ?>" />
          <h2><?php the_sub_field('insta_heading_text'); ?></h2>
        </div>

        <?php the_sub_field('instagram_feed') ?>

      <!-- copy -->

    <?php endwhile; ?>
    <?php endif; ?>
  <?php endwhile; ?>

</section>
<?php endif; ?>
<!-- end why career conference -->



  </article>

<?php get_footer(); ?>
