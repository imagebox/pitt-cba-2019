<?php
/**
 * The template for displaying the Blog page.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

get_header(); ?>

<?php include_once('template-parts/mobile-cat-nav.php'); ?>

<?php

/**
 * Displays an article card
 */


$post_feature = get_the_post_thumbnail( get_the_ID(), 'home_slideshow', '' );
$card_terms = get_the_terms( get_the_ID(), 'category' );
$card_term_name = '';
$card_term_slug = '';

if ( $card_terms && ! is_wp_error( $card_terms )) {
  foreach ( $card_terms as $term ) {
    $card_term_name = $term->name;
    $card_term_slug = $term->slug;
  }
}

 ?>

    <?php
    if ( !is_paged() ) {
      $sticky = get_option( 'sticky_posts' );
      $args = array(
            'posts_per_page' => 1,
            'post__in'  => $sticky,
            'ignore_sticky_posts' => 1
      );
      $query = new WP_Query( $args );
      if ( isset( $sticky[0] ) ) {?>

      <div class="featured-post feature-desktop-hide-show">

          <?php
          $feature_image_bg      = get_field( 'feature_image_bg' );
          $feature_image_bg_size = 'feature_banner_news';
           ?>

           <?php echo $post_feature; ?>

            <img class="feature-post-bgimage"
            src="<?php echo esc_url( $feature_image_bg['sizes'][ $feature_image_bg_size ] ); ?>"
            width="<?php echo esc_attr( $feature_image_bg['sizes'][ $feature_image_bg_size . '-width'] ); ?>"
            height="<?php echo esc_attr( $feature_image_bg['sizes'][ $feature_image_bg_size . '-height'] ); ?>"
            alt="<?php echo esc_attr( $feature_image_bg['alt'] ); ?>">

    <a href="<?php the_permalink(); ?>">
            <div class="card-article">
                <div class="card-article-body">
                  <div class="feature-wrap">
                  <?php if ( ! empty( $card_term_name ) && ! empty( $card_term_slug )) : ?>
                      <div class="feature-icon">
                        <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                          <use xlink:href="#icon-<?php echo $term->slug; ?>">
                        </svg>
                      </div>
                      <h5 class="card-article-cat"><?php echo $card_term_name; ?></h5>
                    <?php endif; ?>

                    <h3 class="card-article-title"><?php the_title(); ?></h3>
                    <div class="readmore-block">
                      <span>Read More</span>
                      <svg class="" width="20" height="20">
                        <use xlink:href="#feature-more"></use>
                      </svg>
                    </div>
                  </div>
              </div>
          </div>
        </a>
        </div>
      </a>
        <div class="featured-post-mobile feature-mobile-hide-show">
            <div class="card-article">
                <div class="card-article-body">
                  <div class="feature-wrap">
                  <?php if ( ! empty( $card_term_name ) && ! empty( $card_term_slug )) : ?>
                    <div class="feature-icon">
                      <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                        <use xlink:href="#icon-<?php echo $term->slug; ?>">
                      </svg>
                    </div>
                      <h5 class="card-article-cat"><?php echo $card_term_name; ?></h5>
                    <?php endif; ?>
                  <a href="<?php the_permalink(); ?>">
                    <h3 class="card-article-title"><?php the_title(); ?></h3>
                    <div class="readmore-block">
                      <span>Read More</span>
                      <svg class="" width="20" height="20">
                        <use xlink:href="#feature-more"></use>
                      </svg>
                    </div>
                  </a>
                  </div>
              </div>
          </div>
        </div>

    <?php } wp_reset_query();
    }
  ?>

  <section class="news-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main">

          <header class="vh page-header">
               <h1 class="page-title"><?php _e('News', 'boxpress'); ?></h1>
           </header>

            <?php
            $regular_post_query = array(
              'posts_per_page' => 9,
              'post__not_in' => get_option( 'sticky_posts' ),
              'paged' => $paged,
            );
            $regular_post_loop = new WP_Query( $regular_post_query );
            ?>
            <div class="l-grid-wrap">
              <div id="ajax-load-more-container" class="l-grid l-grid--three-col l-grid--gutter-small">


                <?php  while ( $regular_post_loop->have_posts() ) : $regular_post_loop->the_post();?>

                  <div class="l-grid-item">
                    <?php get_template_part( 'template-parts/cards/card-article' ); ?>
                  </div>

                <?php
                  endwhile;
                ?>

            </div>
          </div>

          <?php imagebox_numeric_posts_nav(); ?>

          <div class="popular-post-mobile">

            <?php
              add_filter( 'get_search_form', 'extra_search_form' );
              get_search_form();
              remove_filter( 'get_search_form', 'extra_search_form' );
            ?>


            <?php
                /**
                 * Blog Archive Links
                 */

                $blog_archives = wp_get_archives( array(
                  'type'            => 'monthly',
                  'format'          => 'option',
                  'echo'            => false,
                ));
              ?>

              <?php if ( $blog_archives ) : ?>

                <div class="sidebar-widget">
                  <h4 class="widget-title archive-title"><?php _e('Archives', 'boxpress'); ?></h4>
                  <div class="archives-widget news-select">
                    <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                      <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
                      <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                        <option value=""><?php echo _e( 'Select Month' ); ?></option>
                        <?php echo $blog_archives; ?>
                      </select>
                    </form>
                  </div>
                </div>

              <?php endif; ?>


          </div>

        </div>
        <div class="l-aside">

          <?php get_sidebar(); ?>

        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
