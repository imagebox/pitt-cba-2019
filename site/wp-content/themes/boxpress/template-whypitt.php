<?php
/**
 * Template Name: WhyPitt
 */


get_header(); ?>


  <article>

    <?php get_template_part('template-parts/banners/banner--whypitt'); ?>


      <section class="graphic-block-section why-graphic-block-cb">
        <div class="wrap">
          <h3><?php the_field('why_pitt_stat_heading'); ?></h3>
          <p><?php the_field('why_pitt_stat_subheading'); ?></p>

          <?php if( have_rows('why_pitt_graphic_block') ): ?>
            <!-- if why pitt page -->
              <div class="graphic-block">
                <?php while( have_rows('why_pitt_graphic_block') ): the_row();
                  $why_icon_text = get_sub_field('why_stat_icon_text');
                  $why_image = get_sub_field('why_stat_icon');
                 ?>

                  <div class="graphic-block-content pitt-line-graphic-block">
                    <?php if ( $why_image ) : ?>
                      <img src="<?php echo $why_image; ?>" alt="">
                    <?php endif; ?>

                    <?php if ( ! empty($why_icon_text) ) : ?>
                          <h3><?php echo the_sub_field('why_stat_icon_text'); ?></h3>
                    <?php endif; ?>
                  </div>
                  <?php endwhile; ?>
                </div>
            </div>
          </section>
      <?php endif; ?>
      <!-- graphic block -->

    <?php if( have_rows('why_pitt_block') ): ?>
      <?php while( have_rows('why_pitt_block') ): the_row();
        $image = get_sub_field('why_pitt_photo_block');
        $heading = get_sub_field('why_pitt_photo_block_heading');
        $description = get_sub_field('why_pitt_photo_block_description');
        $why_pitt_col_link_1 = get_sub_field('why_pitt_col_link_1');
        $why_pitt_col_link_2 = get_sub_field('why_pitt_col_link_2');
        $why_pitt_col_link_3 = get_sub_field('why_pitt_col_link_3');
        $why_pitt_col_heading_1 = get_sub_field('why_pitt_col_heading_1');
        $why_pitt_col_heading_2 = get_sub_field('why_pitt_col_heading_2');
        $why_pitt_col_heading_3 = get_sub_field('why_pitt_col_heading_3');
        $why_pitt_col_text_1 = get_sub_field('why_pitt_col_text_1');
        $why_pitt_col_text_2 = get_sub_field('why_pitt_col_text_2');
        $why_pitt_col_text_3 = get_sub_field('why_pitt_col_text_3');
        $why_pitt_col_image_1 = get_sub_field('why_pitt_col_image_1');
        $why_pitt_col_image_2 = get_sub_field('why_pitt_col_image_2');
        $why_pitt_col_image_3 = get_sub_field('why_pitt_col_image_3');
        $why_one_third_position = get_sub_field( 'why_one_third_column_position' );
        $why_two_thirds_content = get_sub_field( 'why_two_thirds_content' );
        $why_background = get_sub_field( 'why_background' );
        $why_theme_color_option = get_sub_field( 'why_theme_color_option');
        $why_callout_title    = get_sub_field( 'why_button_block_title' );
        $why_callout_copy     = get_sub_field( 'why_button_block_copy' );
        $why_button_link      = get_sub_field( 'why_button_block_button_link' );
        $why_pitt_photo_block_position = get_sub_field( 'why_pitt_photo_block_position' );
        $circle_image_border = get_sub_field('circle_image_border');
        $quote_border = get_sub_field('quote_border');
        $quote_name = get_sub_field('quote_name');
        $quote_title = get_sub_field('quote_title');
        $quote_line = get_sub_field('quote_line');
       ?>
      <div class="why-border why-color-option <?php echo $why_theme_color_option; ?>"></div>
      <section class="why-pitt-photo-block bg-switch bg-switch--<?php echo $why_pitt_photo_block_position; ?>">
        <div class="wrap">
          <div class="photo-switch photo-switch--<?php echo $why_pitt_photo_block_position; ?>">





            <?php if ( $image ) :
                  $image_url  = $image['sizes'][ 'why_pitt_photo_block' ];
                  $image_w    = $image['sizes'][ 'why_pitt_photo_block' . '-width' ];
                  $image_h    = $image['sizes'][ 'why_pitt_photo_block' . '-height' ];
              ?>
                <img class="why-pitt-photo bkg-image" src="<?php echo $image_url; ?>"
                  width="<?php echo  $image_w; ?>"
                  height="<?php echo $image_h; ?>"
                  alt="">
            <?php endif; ?>


             <div class="why-pitt-block-content">
               <div class="button-block-header">
                 <h2><?php the_sub_field('why_pitt_photo_block_heading'); ?></h2>
                 <p><?php the_sub_field('why_pitt_photo_block_description'); ?></p>
               </div>
             </div>
          </div>
          </div>
      </section>
      <!-- photo block -->


      <section class="why-pitt-column-block">
        <div class="wrap">
          <div class="why-pitt-col">
              <?php if ( $why_pitt_col_image_1 ) : ?>
                <img class="" src="<?php echo $why_pitt_col_image_1['url']; ?>" alt="">
              <?php endif; ?>


              <h6><?php echo $why_pitt_col_heading_1 ?></h6>
                      <div class="pitt-col-text">
              <p><?php echo $why_pitt_col_text_1; ?></p>



            <?php if( $why_pitt_col_link_1  ): ?>
              <a class="button why-color-option <?php echo $why_theme_color_option; ?>"  href="<?php echo $why_pitt_col_link_1['url']; ?>" target="<?php echo $why_pitt_col_link_1['target']; ?>"><?php echo $why_pitt_col_link_1['title']; ?></a>
            <?php endif; ?>
              </div>
          </div>

          <div class="why-pitt-col">

              <?php if ( $why_pitt_col_image_2 ) : ?>
                <img class="" src="<?php echo $why_pitt_col_image_2['url']; ?>" alt="">
              <?php endif; ?>


                <h6><?php echo $why_pitt_col_heading_2 ?></h6>
                 <div class="pitt-col-text">
                <p><?php echo $why_pitt_col_text_2; ?></p>

                <?php if( $why_pitt_col_link_2  ): ?>
                      <a class="button why-color-option <?php echo $why_theme_color_option; ?>" href="<?php echo $why_pitt_col_link_2['url']; ?>" target="<?php echo $why_pitt_col_link_2['target']; ?>"><?php echo $why_pitt_col_link_2['title']; ?></a>
                <?php endif; ?>
                </div>
          </div>

          <div class="why-pitt-col">
              <?php if ( $why_pitt_col_image_3 ) : ?>
                <img class="" src="<?php echo $why_pitt_col_image_3['url']; ?>" alt="">
              <?php endif; ?>

              <h6><?php echo $why_pitt_col_heading_3 ?></h6>
                    <div class="pitt-col-text">
              <p><?php echo $why_pitt_col_text_3; ?></p>
            <?php if( $why_pitt_col_link_3  ): ?>
                <a class="button why-color-option <?php echo $why_theme_color_option; ?>" href="<?php echo $why_pitt_col_link_3['url']; ?>" target="<?php echo $why_pitt_col_link_3['target']; ?>"><?php echo $why_pitt_col_link_3['title']; ?></a>
            <?php endif; ?>
            </div>
          </div>
        </div>
      </section>
        <!-- column block -->

          <section class="fullwidth-column section <?php echo $why_background; ?>">
              <div class="wrap wrap--limited">
                <div class="columns-two-thirds why-quote  columns-two-thirds--<?php echo $why_one_third_position; ?>">
                  <div class="col-two-third <?php echo $why_background; ?>">
                    <div class="page-content why-color-option quote-border <?php echo $why_theme_color_option; ?> <?php echo $quote_border; ?>">
                      <?php echo $why_two_thirds_content; ?>
                      <p class="quote-name"><?php echo $quote_name; ?></p>
                      <p class="quote-title"><?php echo $quote_title; ?></p>
                    </div>
                  </div>
                  <div class="col-one-third">
                    <div class="page-content">
                      <?php $quote_image = get_sub_field('quote_image'); ?>


                          <?php if ( $quote_image ) :
                              $quote_image_url  = $quote_image['sizes'][ 'quote_image' ];
                              $quote_image_w    = $quote_image['sizes'][ 'quote_image' . '-width' ];
                              $quote_image_h    = $quote_image['sizes'][ 'quote_image' . '-height' ];
                            ?>

                  <div class="quote-circle <?php echo $circle_image_border; ?>" style="background-image: url(<?php echo esc_url($quote_image_url); ?>); background-repeat: no-repeat; background-size: cover; background-position: center center; width='<?php echo  $quote_image_w; ?>' height='<?php echo $quote_image_h; ?>' ">

                    <div class="quote-inner-circle <?php echo $quote_border; ?>">
                      <img src="<?php bloginfo('template_directory'); ?>/assets/img/quote-circle.png" />
                    </div>
                      <?php endif; ?>

                    </div>

                  </div>
                </div>
              </div>
          </section>
        <!-- quote block -->

        <section class="fullwidth-column section why-button-block <?php echo $why_theme_color_option; ?>">

          <div class="wrap wrap--limited">
            <div class="button-block">

              <?php if ( ! empty( $why_callout_title )) : ?>

                <div class="callout-header">
                  <h2><?php echo $why_callout_title; ?></h2>
                </div>

              <?php endif; ?>

              <div class="callout-body">
                <div class="button-callout-content">

                  <?php if ( $why_button_link ) : ?>
                    <a class="button"
                      href="<?php echo esc_url( $why_button_link['url'] ); ?>"
                      target="<?php echo $why_button_link['target']; ?>">
                      <?php echo $why_button_link['title']; ?>
                    </a>
                  <?php endif; ?>

                </div>
              </div>
            </div>
          </div>
        </section>
    <!-- Button block -->

        <?php endwhile; ?>
    <?php endif; ?>
    <!-- why pitt block -->

  </article>

<?php get_footer(); ?>
