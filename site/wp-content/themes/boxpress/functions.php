<?php

/**
 * BoxPress functions and definitions
 *
 * @package BoxPress
 */

if ( ! function_exists( 'boxpress_setup' )) :
function boxpress_setup() {

  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails', array( 'post', 'page', 'careers' ));
  add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ));

  add_image_size( 'home_slideshow', 1200, 600, true );
  add_image_size( 'feature_banner_news', 1200, 600, true );
  add_image_size( 'home_index_thumb', 860, 515, true );
  add_image_size( 'headshot_image', 100, 100, array( 'center', 'top'), true );
  add_image_size( 'admission_req_image', 280, 177, true);
  add_image_size( 'background_image_block_career_conference_media', 580, 400, true);
  add_image_size( 'background_image_block_about_media', 580, 400, true);
  add_image_size( 'background_image_block_why_media', 580, 400, true);
  add_image_size( 'image_apply', 380, 304, true);
  add_image_size( 'logo_block_media', 90, 80, true);
  add_image_size( 'background_image_block_admission_ov', 580, 400, true);
  add_image_size( 'why_pitt_photo_block', 580, 400, true);
  add_image_size( 'quote_image', 231, 196, true);
  add_image_size( 'video_block_media', 580, 400, true);
  add_image_size( 'carousel_slides_size', 1000, 680, array( 'center', 'top'), true );
  add_image_size( 'carousel_thumb', 60, 60, true );
  add_image_size( 'card_thumb', 600, 401, true );
  add_image_size( 'banner_image', 553, 368, true );
  add_image_size( 'block_half_width', 800, 534, true );
  // Blog
  add_image_size( 'article_thumb', 808, 515, true );

  // Cards
  add_image_size( 'card_post_thumb', 287, 192, true );
  add_image_size( 'card_vendor_thumb', 382, 348, true );
  add_image_size( 'card_vendor_thumb_small', 285, 210, true );

  // Mosaic
  add_image_size( 'mosaic_image', 500, 436, true );


  add_filter( 'admin_post_thumbnail_html', 'add_featured_image_instruction');
function add_featured_image_instruction( $content ) {
    return $content .= '<p><strong>Recommended Image Size:</strong> 808x505</p>';
}




}
endif;
add_action( 'after_setup_theme', 'boxpress_setup' );



/**
 * Enqueue scripts and styles.
 */

function boxpress_scripts() {

  /**
   * Styles
   */

  $style_font_url     = 'https://fonts.googleapis.com/css?family=Lato:400,700,700i,900|Raleway:400,700';
  $style_screen_path  = get_stylesheet_directory_uri() . '/assets/css/style.min.css';
  $style_screen_ver   = filemtime( get_stylesheet_directory() . '/assets/css/style.min.css' );
  $style_print_path   = get_stylesheet_directory_uri() . '/assets/css/print.css';
  $style_print_ver    = filemtime( get_stylesheet_directory() . '/assets/css/print.css' );

  wp_enqueue_style( 'google-fonts', $style_font_url, array(), false, 'screen' );
  wp_enqueue_style( 'screen', $style_screen_path, array('google-fonts'), $style_screen_ver, 'screen' );
  wp_enqueue_style( 'print', $style_print_path, array( 'screen' ), $style_print_ver, 'print' );


  /**
   * Scripts
   */

  $script_modernizr_path  = get_template_directory_uri() . '/assets/js/dev/modernizr.js';
  $script_modernizr_ver   = filemtime( get_template_directory() . '/assets/js/dev/modernizr.js' );
  $script_site_path = get_template_directory_uri() . '/assets/js/build/site.min.js';
  $script_site_ver  = filemtime( get_template_directory() . '/assets/js/build/site.min.js' );

  wp_enqueue_script( 'modernizr', $script_modernizr_path, array(), $script_modernizr_ver, false );
  wp_enqueue_script( 'site', $script_site_path, array( 'jquery' ), $script_site_ver, true );

  // Single Posts
  if ( is_singular() && comments_open() &&
       get_option( 'thread_comments' )) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'boxpress_scripts' );



/**
 * Custom Functions
 */

function query_for_child_page_list() {
  global $wp_query;
  $post = $wp_query->post;

  if ( $post ) {
    $ancestors  = get_post_ancestors( $post );
    $parent     = end( $ancestors );

    if ( empty( $post->post_parent )) {
      $parent = $post->ID;
    }

    // Get list of pages
    return wp_list_pages( array(
      'title_li'  => '',
      'child_of'  => $parent,
      'depth'     => 4,
      'echo'      => false,
    ));
  }
}


/**
 * Menu setup
 */
require get_template_directory() . '/inc/site-navigation.php';

/**
 * Accessibility
 */
require get_template_directory() . '/inc/walkers/class-aria-walker-nav-menu.php';

/**
 * Cleanup the header
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Admin Related Functions
 */
require get_template_directory() . '/inc/admin.php';
require get_template_directory() . '/inc/editor.php';

/**
 * ACF
 */
// Options Pages
require get_template_directory() . '/inc/acf-options.php';
// Search Queries
require get_template_directory() . '/inc/acf-search.php';

/**
 * Plugin Settings
 */
require get_template_directory() . '/inc/plugin-settings/gravity-forms.php';
require get_template_directory() . '/inc/plugin-settings/tribe-events.php';

/**
 * STAFF CPT
 */

require get_template_directory() . '/inc/cpt/staff.php';
require get_template_directory() . '/inc/cpt/eir.php';
// require get_template_directory() . '/inc/cpt/tax-global.php';

/**
 * CAREERS bloginfo
 */

require get_template_directory() . '/inc/cpt/careers-blog.php';

/**
 * Custom excerpt
 */

function custom_short_excerpt($excerpt){
	return substr($excerpt, 0, 120);
}
add_filter('the_excerpt', 'custom_short_excerpt');


/**
 * Search Functions
 */

// Sitewide Search
function main_search_form( $form ) {
    $form = '<form role="search" class="search-form" method="get" id="search-form" action="'. home_url( '/' ) .'">';
    $form .= '<label><span class="screen-reader-text">Search</span>';
    $form .= '<input name="s" class="search-field" id="search" type="search" value="" placeholder="Search" title="Search">';
    $form .= '</label>';
    $form .= '<input type="submit" value="" class="search-submit">';
    $form .= '</form>';
    return $form;
}
add_filter( 'get_search_form', 'main_search_form' );


// News / Blog Search
function extra_search_form( $form ) {
    $form = '<form role="search" class="search-form news-search-form" method="get" id="search-form" action="'. home_url( '/' ) .'">';
    $form .= '<label><span class="screen-reader-text">Search</span>';
    $form .= '<input name="s" class="search-field" id="search" type="search" value="" placeholder="Search News" title="Search">';
    $form .= '</label>';
    $form .= '<input type="hidden" name="post_type" value="post" />';
    $form .= '<input type="submit" value="" class="search-submit">';
    $form .= '</form>';
    return $form;
}

// Careers / Blog Search
function extra_search_form_careers( $form ) {
    $form = '<form role="search" class="search-form news-search-form" method="get" id="search-form" action="'. home_url( '/' ) .'">';
    $form .= '<label><span class="screen-reader-text">Search Blog</span>';
    $form .= '<input name="s" class="search-field" id="search" type="search" value="" placeholder="Search Blog" title="Search">';
    $form .= '</label>';
    $form .= '<input type="hidden" name="post_type" value="careers" />';
    $form .= '<input type="submit" value="" class="search-submit">';
    $form .= '</form>';
    return $form;
}

/*******************************************************************************************/
/*	Pagination Addon						      				 						   */
/*******************************************************************************************/

	function imagebox_numeric_posts_nav() {

		if( is_singular() )
			return;

		global $wp_query;

		/** Stop execution if there's only 1 page */
		if( $wp_query->max_num_pages <= 1 )
			return;

		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max   = intval( $wp_query->max_num_pages );

		/**	Add current page to the array */
		if ( $paged >= 1 )
			$links[] = $paged;

		/**	Add the pages around the current page to the array */
		if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
		}

		if ( ( $paged + 2 ) <= $max ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
		}

		echo '<div class="pagination"><ul>' . "\n";

		/**	Previous Post Link */
		if ( get_previous_posts_link() )
			printf( '<li class="previous-page">%s</li>' . "\n", get_previous_posts_link('<svg class="pagination-svg-prev" width="28" height="28">
              <use xlink:href="#pagination-left-arrow"></use>
            </svg> Previous') );

		/**	Link to first page, plus ellipses if necessary */
		if ( ! in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="active"' : '';

			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

			if ( ! in_array( 2, $links ) )
				echo '<li class="dotdotdot">…</li>';
		}

		/**	Link to current page, plus 2 pages in either direction if necessary */
		sort( $links );
		foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
		}

		/**	Link to last page, plus ellipses if necessary */

		/**	Next Post Link */
		if ( get_next_posts_link() )
			printf( '<li class="next-page">%s</li>' . "\n", get_next_posts_link('Next <svg class="pagination-svg" width="28" height="28">
              <use xlink:href="#pagination-right-arrow"></use>
            </svg>') );

		echo '</ul></div>' . "\n";

	}


  // filter to allow post type argument in wp_get_archives
  function my_custom_post_type_archive_where($where,$args) {
      $post_type  = isset($args['post_type'])  ? $args['post_type']  : 'post';
      $where = "WHERE post_type = '$post_type' AND post_status = 'publish'";
      return $where;
  }
  add_filter( 'getarchives_where','my_custom_post_type_archive_where',10,2);
