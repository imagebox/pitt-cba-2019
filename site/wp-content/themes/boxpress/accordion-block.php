<?php
/**
 * Displays the Accordion Layout
 *
 * @package BoxPress
 */

$background = get_sub_field( 'background' );
$accord_heading = get_sub_field('accord_heading');


?>


<section class="fullwidth-column section <?php echo $background; ?>">
  <div class="wrap accordion-wrap">
 <h2><?php echo $accord_heading ?></h2>
    <?php if ( have_rows( 'accordion_row' ) ) : ?>
      <?php while ( have_rows( 'accordion_row' ) ) : the_row(); ?>

    <button class="accordion accord-is-open"><?php the_sub_field('accord_title'); ?></button>
    <div class="accordion-content">
      <p><?php the_sub_field('accord_content'); ?></p>
    </div>

    <?php endwhile; endif; ?>
  </div>
</section>
