<?php
/**
 * The template for displaying all single posts.
 *
 * @package BoxPress
 */




 $news_posts = get_field('select_news_post');

 if( $news_posts ) {


 foreach( $news_posts as $news_post ):
   $permalink = get_permalink($news_post->ID );
   wp_redirect($permalink);
 endforeach;

}

$news_post_subtitle  = get_field( 'news_post_subtitle' );
$news_author  = get_field( 'news_author' );
$card_terms = get_the_terms( get_the_ID(), 'careers_categories' );
$post_cat_name = '';
$post_cat_slug = '';

if ( $card_terms && ! is_wp_error( $card_terms )) {
  foreach ( $card_terms as $term ) {
    $post_cat_name = $term->name;
    $post_cat_slug = $term->slug;
  }
}

get_header(); ?>

<?php include_once('template-parts/mobile-cat-nav-career.php'); ?>

  <section class="news-page">
    <div class="wrap">
      <div class="l-sidebar">
        <div class="l-main">

          <?php while ( have_posts() ) : the_post(); ?>

            <div class="post-section">
              <?php // Post Nav ?>
              <?php get_template_part( 'template-parts/post-nav', 'single' ); ?>


              <article id="post-<?php the_ID(); ?>" <?php post_class( 'content--single' ); ?>>
                <header class="entry-header">
                  <div class="entry-header-body">
                    <div class="small-wrap">
                      <?php if ( ! empty( $post_cat_name )) : ?>
                        <h5 class="entry-header-cat news-post-title-cat"><?php echo $post_cat_name; ?></h5>
                      <?php endif; ?>

                      <h1 class="entry-title"><?php the_title(); ?></h1>

                      <?php if ( has_post_thumbnail() ) : ?>
                      <div class="post-header-thumb">
                        <?php the_post_thumbnail( 'article_thumb' ); ?>
                      </div>
                      </div>
                      <!-- small wrap  -->

                      <div class="small-wrap">
                        <?php if ($news_post_subtitle): ?>
                          <div class="news-post-subtitle">
                            <h6><?php echo $news_post_subtitle;  ?></h6>
                          </div>
                        <?php endif; ?>

                        <div class="news-author-date-social">
                          <div class="news-author-date">
                            <?php if( $news_author ): ?>
                              <h6 class="news-author">By <?php echo $news_author; ?></h6>
                            <?php endif; ?>
                            <h6 class="news-date"><?php echo get_the_date(); ?></h6>
                          </div>
                          <?php include( get_template_directory() . '/template-parts/social-share.php' ); ?>
                        </div>
                      </div>

                      <?php endif; ?>

                  </div>
                </header>

                <?php // Advanced Post Content Blocks ?>
                <?php if ( have_rows( 'master_post' )) : $row_index = 1; ?>
                  <div class="small-wrap">
                    <div class="entry-content">

                      <?php while ( have_rows( 'master_post' )) : the_row(); ?>

                        <?php
                        // Create layout string
                        $layout_path = 'template-layouts/' . str_replace( '_', '-', get_row_layout()) . '.php';

                        // Include our layout
                        if (( @include $layout_path ) === false ) {
                          // If the layout block is missing, display an error block only for admins
                          if ( current_user_can('edit_others_pages') ) {
                            echo '<div class="section boxpress-error"><div class="wrap wrap--limited"><p>Sorry bud, that layout file is missing!</p></div></div>';
                          }
                        }

                        $row_index++;
                        ?>

                      <?php endwhile; ?>

                    </div>
                  </div>

                  <?php wp_reset_postdata(); ?>
                <?php endif; ?>

                <footer class="entry-footer">
                  <?php // include( get_template_directory() . '/template-parts/social-share.php' ); ?>
                </footer>
              </article>

              <?php // Post Nav ?>
              <?php get_template_part( 'template-parts/post-nav', 'single' ); ?>

          <div class="small-wrap">
            <div class="news-back-button">
                <a href="/blog/"><svg width="27" height="27"><use xlink:href="#back-arrow"></svg><span><?php _e( 'Back to Careers', 'boxpress' ); ?></span></a>
            </div>
          </div>

            </div>


            <?php if(get_field('related_posts')) { ?>

                <?php

                $posts = get_field('related_posts');

                if( $posts ): ?>

                <div class="related-wrap">
                  <div class="post-section related-post-section">
                    <h3 class="related-post">Related</h3>
                    <div class="l-grid-wrap">
                      <div class="l-grid l-grid--three-col l-grid--gutter-small">





                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                        <?php setup_postdata($post); ?>
                        <div class="l-grid-item">
                          <?php get_template_part( 'template-parts/cards/card-article' ); ?>
                        </div>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>
            </div>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                <?php endif; ?>

            <?php } else { ?>



              <?php // Related Posts ?>
              <?php
                $current_post_cats  = get_the_terms( get_the_ID(), 'careers_categories' );
                $current_post_cat   = array();

                if ( $current_post_cats && ! is_wp_error( $current_post_cats ) ) {
                  foreach ( $current_post_cats as $term ) {
                    $current_post_cat[] = $term->term_id;
                  }
                }

                $current_post = get_the_ID();

                $related_posts_query_args = array(
                  'post_type' => 'careers',
                  'posts_per_page' => 3,
                  'post__not_in' => array($current_post),
                  'orderby'        => 'rand',
                  'tax_query' => array(
                      array(
                        'taxonomy' => 'careers_categories',
                        'field'    => 'term_id',
                        'terms'    => $current_post_cat,
                      ),
                    ),
                );
                $related_posts_query = new WP_Query( $related_posts_query_args );
              ?>
              <?php if ( $related_posts_query->have_posts() ) : ?>

                <div class="related-wrap">
                  <div class="post-section related-post-section">
                    <h3 class="related-post">Related</h3>
                    <div class="l-grid-wrap">
                      <div class="l-grid l-grid--three-col l-grid--gutter-small">

                        <?php while ( $related_posts_query->have_posts() ) : $related_posts_query->the_post(); ?>

                          <div class="l-grid-item">
                            <?php get_template_part( 'template-parts/cards/card-careers-article' ); ?>
                          </div>

                        <?php endwhile; ?>

                      </div>
                    </div>
                  </div>
                </div>

                <?php wp_reset_postdata(); ?>
              <?php endif; ?>


            <?php } ?>


          <?php endwhile; ?>

          <div class="related-wrap">
            <div class="popular-post-mobile popular-post-article">
              <?php
                add_filter( 'get_search_form', 'extra_search_form' );
                get_search_form();
                remove_filter( 'get_search_form', 'extra_search_form' );
              ?>


              <?php
                  /**
                   * Blog Archive Links
                   */

                  $blog_archives = wp_get_archives( array(
                    'type'            => 'monthly',
                    'format'          => 'option',
                    'echo'            => false,
                  ));
                ?>

                <?php if ( $blog_archives ) : ?>

                  <div class="sidebar-widget">
                    <h4 class="widget-title archive-title"><?php _e('Archives', 'boxpress'); ?></h4>
                    <div class="archives-widget news-select">
                      <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                        <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
                        <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                          <option value=""><?php echo _e( 'Select Month' ); ?></option>
                          <?php echo $blog_archives; ?>
                        </select>
                      </form>
                    </div>
                  </div>

                <?php endif; ?>

            </div>
          </div>
        </div>
        <div class="l-aside">

          <?php get_sidebar('careers'); ?>

        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
