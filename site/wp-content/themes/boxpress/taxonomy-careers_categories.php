<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

get_header(); ?>

<?php require_once('template-parts/banners/banner--archive.php'); ?>

<?php include_once('template-parts/mobile-cat-nav.php'); ?>

<?php

/**
 * Displays an article card
 */

$post_feature = get_the_post_thumbnail( get_the_ID(), 'home_slideshow', '' );
$card_terms = get_the_terms( get_the_ID(), 'careers_categories' );

$card_term_name = '';
$card_term_slug = '';

if ( $card_terms && ! is_wp_error( $card_terms )) {
  foreach ( $card_terms as $term ) {
    $card_term_name = $term->name;
    $card_term_slug = $term->slug;
  }
}


 ?>



  <section class="news-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main">

          <header class="vh page-header">
               <h1 class="page-title"><?php _e('News', 'boxpress'); ?></h1>
           </header>

          <?php
        /**
         * Featured Posts
         */

        $query_featured_posts_args = array(
          'post_type' => 'careers',
          'posts_per_page' => 3,
          'tax_query' => array(
            array(
              'taxonomy'  => 'featured_post',
              'field'     => 'slug',
              'terms'     => 'blog-page',
            ),
          ),
        );
        $query_featured_posts = new WP_Query( $query_featured_posts_args );
      ?>
      <?php if ( $query_featured_posts->have_posts() ) : ?>

        <div class="blog-featured-posts">
          <div class="js-hero-carousel owl-carousel owl-theme">

            <?php while ( $query_featured_posts->have_posts() ) : $query_featured_posts->the_post();
                $card_terms = get_the_terms( get_the_ID(), 'category' );
              ?>

              <div class="featured-post-slide">
                <?php if ( has_post_thumbnail() ) : ?>
                  <?php the_post_thumbnail( 'article_thumb' ); ?>
                <?php endif; ?>

                <div class="featured-post-content">
                  <?php if ( $card_terms && ! is_wp_error( $card_terms )) : ?>
                    <h4>
                      <?php foreach ( $card_terms as $term ) : ?>
                        <span><?php echo $term->name; ?></span>
                      <?php endforeach; ?>
                    </h4>
                  <?php endif; ?>
                  <h3><?php the_title(); ?></h3>

                </div>
              </div>

            <?php endwhile; ?>

          </div>
        </div>

        <?php wp_reset_postdata(); ?>
      <?php endif; ?>


      <?php if ( have_posts() ) : ?>

        <div class="l-grid-wrap">
          <div id="ajax-load-more-container" class="l-grid l-grid--three-col l-grid--gutter-small">

            <?php while ( have_posts() ) : the_post(); ?>

              <div class="l-grid-item">
                <?php get_template_part( 'template-parts/cards/card-careers-article' ); ?>
              </div>

            <?php endwhile; ?>

          </div>
        </div>
        <?php imagebox_numeric_posts_nav(); ?>

        <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

      <?php endif; ?>

      <div class="popular-post-mobile">
        <?php
          add_filter( 'get_search_form', 'extra_search_form' );
          get_search_form();
          remove_filter( 'get_search_form', 'extra_search_form' );
        ?>


        <?php
            /**
             * Blog Archive Links
             */

            $blog_archives = wp_get_archives( array(
              'type'            => 'monthly',
              'format'          => 'option',
              'echo'            => false,
            ));
          ?>

          <?php if ( $blog_archives ) : ?>

            <div class="sidebar-widget">
              <h4 class="widget-title archive-title"><?php _e('Archives', 'boxpress'); ?></h4>
              <div class="archives-widget news-select">
                <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                  <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
                  <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                    <option value=""><?php echo _e( 'Select Month' ); ?></option>
                    <?php echo $blog_archives; ?>
                  </select>
                </form>
              </div>
            </div>

          <?php endif; ?>

        <h4>TRENDING STORIES</h4>
          <?php
            if (function_exists('wpp_get_mostpopular'))
            $args = array(
            'post_html' => '<li>{thumb} {title}</li>',
            'thumbnail_width' => 105,
            'thumbnail_height' => 71,
            'post_type' => 'careers',
            'limit' => 4,
            );
                wpp_get_mostpopular($args);
          ?>
      </div>

    </div>
        <div class="l-aside">

          <?php get_sidebar('careers'); ?>

        </div>
      </div>
    </div>
  </section>



<?php get_footer(); ?>
