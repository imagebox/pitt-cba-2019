<?php
/**
 * Template Name: EIR
 */
get_header(); ?>


  <article class="homepage">

    <?php get_template_part('template-parts/banners/banner--contact'); ?>

    <section class="fullwidth-column advanced-full-width section <?php echo $background; ?>">
      <div class="wrap">

        <div class="l-sidebar">
          <div class="l-main">

            <?php $eir_content = get_field('eir_content'); ?>

            <?php echo $eir_content; ?>

            <?php
              $staff_query_args = array(
                'post_type' => 'eir',
                'posts_per_page' => -1,
              );
              $staff_query = new $wp_query( $staff_query_args );
            ?>

            <?php if ( $staff_query->have_posts() ) : ?>

              <section class="section eir-staff contact-associate-dean-block-photos">
                <div class="wrap">



                <div class="l-grid l-grid--three-col">

                    <?php while ( $staff_query->have_posts() ) : $staff_query->the_post(); ?>

                        <div class="l-grid-item">
                        <?php get_template_part( 'template-parts/staff-member' ); ?>
                        </div>

                    <?php endwhile; ?>
                </div>
              <?php wp_reset_postdata(); ?>
            <?php endif; ?>

          </div>
          </section>
          </div>

            <div class="l-aside">
              <?php get_sidebar(); ?>
            </div>
        </div>

      </div>
    </section>
  </article>

<?php get_footer(); ?>
