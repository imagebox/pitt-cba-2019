<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
<script src="//code.tidio.co/pm3bhyrnb33ozb1dp6xgdsuqijcgcidr.js"></script>
  <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php // SVGs ?>
<?php include_once('template-parts/svg.php'); ?>

<?php // Mobile Header ?>
<?php include_once('template-parts/header-mobile.php'); ?>

<header id="masthead" class="site-header" role="banner">
  <div class="wrap">
    <div class="site-header-inner">

      <div class="top-bar">

        <nav class="js-accessible-menu navigation--main"
          aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
          role="navigation">
                <img src="<?php bloginfo('template_directory'); ?>/assets/img/branding/top-logo.png" />
          <ul class="nav-list">
              <?php
                wp_nav_menu( array(
                  'theme_location'  => 'topone',
                  'items_wrap'      => '%3$s',
                  'container'       => false,
                  'walker'          => new Aria_Walker_Nav_Menu(),
                ));
              ?>
          </ul>
          <ul class="nav-list toptwo-nav">
              <?php
                wp_nav_menu( array(
                  'theme_location'  => 'toptwo',
                  'items_wrap'      => '%3$s',
                  'container'       => false,
                  'walker'          => new Aria_Walker_Nav_Menu(),
                ));
              ?>
          </ul>
        </nav>
      </div>


<!-- end top menu -->
<div class="main-nav-block">
  <div class="header-col-1">

    <div class="site-branding">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <svg class="site-logo" width="234" height="71">
            <use xlink:href="#pitt-logo-header"></use>
        </svg>
      </a>
    </div>

  </div>

  <div class="header-col-2">

    <?php if ( has_nav_menu( 'primary' )) : ?>
      <nav class="js-accessible-menu navigation--main"
        aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
        role="navigation">
        <ul class="nav-list">
            <?php
              wp_nav_menu( array(
                'theme_location'  => 'primary',
                'items_wrap'      => '%3$s',
                'container'       => false,
                'walker'          => new Aria_Walker_Nav_Menu(),
              ));
            ?>
        </ul>

        <?php get_search_form();?>

      </nav>
    <?php endif; ?>

  </div>

    <!-- end main menu -->
</div>

      <!-- end secondary menu -->

    </div>
  </div>
  <div class="secondary-nav-block">
    <div class="wrap">
      <nav class="js-accessible-menu  navigation--secondary"
        aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
        role="navigation">
        <ul class="nav-list navigation--secondary">
            <?php
              wp_nav_menu( array(
                'theme_location'  => 'secondary',
                'items_wrap'      => '%3$s',
                'container'       => false,
                'walker'          => new Aria_Walker_Nav_Menu(),
              ));
            ?>
        </ul>
        <?php if ( has_nav_menu( 'button' )) : ?>
          <ul class="nav-list nav-button">
            <?php
            wp_nav_menu( array(
              'theme_location'  => 'button',
              'items_wrap'      => '%3$s',
              'container'       => false,
              'walker'          => new Aria_Walker_Nav_Menu(),
            ));
            ?>
          </ul>
        <?php endif; ?>
      </nav>
    </div>
  </div>
</header>

<main id="main" class="site-main" role="main">
