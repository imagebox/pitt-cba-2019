<?php
/**
 * Template Name: Redirect to Child
 *
 * Displays the redirect template
 *
 * @package BoxPress
 */
if ( have_posts() ) {
  while ( have_posts() ) {
    the_post();
    $children = get_pages( "child_of=" . $post->ID . "&sort_column=menu_order" );

    if ( $children ) {
      wp_redirect( get_permalink( $children[0]->ID ), 301 );
      exit;

    } else {
      wp_redirect( home_url(), 301 );
      exit;
    }
  }
}
?>
