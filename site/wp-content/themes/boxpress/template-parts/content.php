<?php
/**
 * @package BoxPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
    <?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

    <?php if ( 'post' == get_post_type() ) : ?>
    <?php
      if ( has_post_thumbnail() ) {?>
        <a href="<?php the_permalink();?>" rel="bookmark">
          <?php the_post_thumbnail('home_index_thumb');?>
        </a>
      <?php } else { ?>
        <!-- no thumbnail -->
      <?php }
    ?>
    <div class="entry-meta">
      <?php boxpress_posted_on(); ?>
    </div><!-- .entry-meta -->
    <?php endif; ?>
  </header><!-- .entry-header -->

    <?php
      /* translators: %s: Name of current post */
      the_content();
    ?>

    <?php
      wp_link_pages( array(
        'before' => '<div class="page-links">' . __( 'Pages:', 'boxpress' ),
        'after'  => '</div>',
      ) );
    ?>


  <footer class="entry-footer">
    <a class="button button-margin" href="<?php the_permalink();?>">
      Read More
    </a>
    <?php boxpress_entry_footer(); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->
