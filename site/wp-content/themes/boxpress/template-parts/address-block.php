<?php
/**
 * Displays the formatted address
 *
 * @package BoxPress
 */
?>
<div itemscope itemtype="https://schema.org/LocalBusiness">

  <?php if ( have_rows('site_offices', 'option') ) : ?>
    <?php while ( have_rows('site_offices', 'option') ) : the_row();
        $office_title   = get_sub_field('office_title');
        $company        = get_sub_field('company');
        $street_address = get_sub_field('street_address');
        $address_line_2 = get_sub_field('address_line_2');
        $city           = get_sub_field('city');
        $state          = get_sub_field('state');
        $zip            = get_sub_field('zip');
        $country        = get_sub_field('country');
        $phone_number   = get_sub_field('phone_number');
        $email          = get_sub_field('email');
        $fax         = get_sub_field('fax');
      ?>

      <div class="pitt-address" itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">

        <div class="footer-heading-top">
          <?php if ( ! empty( $office_title )) : ?>
            <p class="footer-heading"><?php echo $office_title; ?></p>
          <?php endif; ?>
          <?php if ( ! empty( $company )) : ?>
            <span itemprop="name"><?php echo $company; ?></span><br>
          <?php endif; ?>
        </div>

        <p>
          <?php if ( ! empty( $street_address )) : ?>
            <span itemprop="streetAddress"><?php echo $street_address; ?></span><br>
          <?php endif; ?>

          <?php if ( ! empty( $address_line_2 )) : ?>
            <span><?php echo $address_line_2; ?></span><br>
          <?php endif; ?>

          <?php if ( ! empty( $city )) : ?>
            <span itemprop="addressLocality"><?php echo $city ?></span>,
          <?php endif; ?>

          <?php if ( ! empty( $state )) : ?>
            <span itemprop="addressRegion"><?php echo $state; ?></span>
          <?php endif; ?>

          <?php if ( ! empty( $zip )) : ?>
            <span itemprop="postalCode"><?php echo $zip; ?></span>
          <?php endif; ?>

          <?php if ( ! empty( $country )) : ?>
            <span itemprop="addressCountry"><?php echo $country; ?></span>
          <?php endif; ?>
        </p>


        <?php if ( ! empty( $phone_number ) ||
                   ! empty( $email )) : ?>

          <div class="footer-fax-tel-email">

            <?php if ( ! empty( $phone_number )) :
                // Strip hyphens & parenthesis for tel link
                $tel_formatted = str_replace([ "-", "–", "(", ")" ], '', $phone_number );
              ?>

              <p class="vh"><?php _e( 'Phone:', 'boxpress' ); ?></p>
              <p><a href="tel:+1<?php echo $tel_formatted; ?>" itemprop="telephone"><?php echo $phone_number; ?></a></p>
            <?php endif; ?>

            <?php if ( ! empty( $fax )) : ?>
              <p class="vh"><?php _e( 'Fax:', 'boxpress' ); ?></p>
              <p><?php echo $fax; ?></a></p>
            <?php endif; ?>


            <?php if ( ! empty( $email )) : ?>
              <p class="vh"><?php _e( 'Email:', 'boxpress' ); ?></p>
              <p><a class="email" href="mailto:<?php echo $email; ?>" itemprop="email"><?php echo $email; ?></a></p>
            <?php endif; ?>

          </div>

        <?php endif; ?>

      </div>

    <?php endwhile; ?>
  <?php endif; ?>

</div>
