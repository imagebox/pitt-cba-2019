<?php
/**
 * Displays the Social Navigation
 *
 * @package BoxPress
 */

$group_ID = 152;
$fields = acf_get_fields( $group_ID );

?>
<?php if ( $fields ) : ?>

  <ul class="social-nav">

    <?php foreach ( $fields as $field ) :
        $field_object = get_field_object( $field['name'], 'option' );
      ?>

      <?php if ( $field_object['value'] && ! empty( $field_object['value'] )) : ?>
        <li>
          <a href="<?php echo esc_url( $field_object['value'] ); ?>" target="_blank">
            <span class="vh"><?php echo $field_object['label']; ?></span>
            <div class="new-social-icon">
              <svg class="social-<?php echo $field_object['name']; ?>-svg" width="37" height="37">
                <use xlink:href="#social-<?php echo $field_object['name']; ?>"></use>
              </svg>
            </div>
          </a>
        </li>

      <?php endif; ?>
    <?php endforeach; ?>

  </ul>

<?php endif; ?>
