<?php
/**
 * Hero
 *
 * Displays the Hero Section on the homepage
 *
 * @package BoxPress
 */

// ACF Fields
$hero_background_color = get_field( 'background' );
$hero_background_image = get_field( 'hero_background_image' );

$hero_sub_head 	= get_field( 'hero_subhead' );
$hero_link 			= get_field( 'hero_link' );

$hero_background_video = get_field( 'hero_background_video' );
$hero_background_video_poster_image = get_field( 'hero_background_video_poster_image' );

?>

<section class="hero <?php echo $hero_background_color; ?>"
	<?php
		if ( ! empty( $hero_background_image ) && $hero_background_color == 'background-image' ) {
			echo "style=\"background-image: url({$hero_background_image['url']});\"";
		} elseif ( $hero_background_color === 'video' && $hero_background_video_poster_image ) {
			echo "style=\"background-image:url({$hero_background_video_poster_image['url']})\"";
		}
	?>>
	<div class="wrap">
		<div class="hero-body">

			<h1><?php the_field('hero_heading'); ?></h1>

			<?php if ( ! empty( $hero_sub_head )) : ?>

				<h6><?php echo $hero_sub_head; ?></h6>

			<?php endif; ?>

			<?php if ( ! empty( $hero_link )) : ?>

				<a class="button" href="<?php echo $hero_link; ?>">
					<?php the_field('hero_link_text'); ?>
				</a>

			<?php endif; ?>

		</div>
	</div>

	<?php if ( $hero_background_color === 'video' && $hero_background_video ) : ?>

		<video class="hero-video-bkg" poster="<?php echo $hero_background_video_poster_image['url']; ?>" playsinline autoplay muted loop>
			<source src="<?php echo $hero_background_video['url']; ?>" type="video/mp4">
		</video>

	<?php endif; ?>

</section>
