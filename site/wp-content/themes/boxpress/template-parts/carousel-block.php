  <div class="carousel-wrapper">
    <div class="owl-carousel-block">

      <?php if ( have_rows('carousel') ) : while( have_rows('carousel') ) : the_row(); ?>

        <div class="item <?php if(get_sub_field('link')) { ?>link<?php } else {?>no-link<?php } ?>">

          <?php if ( get_sub_field( 'link' )) : ?>
            <a href="<?php the_sub_field('link'); ?>" target="_blank"> 
          <?php endif; ?>
            
            <img src="<?php the_sub_field('logo'); ?>" alt="">

          <?php if ( get_sub_field( 'link' )) : ?>
            </a>
          <?php endif; ?>
        </div>

      <?php endwhile; endif; ?>

    </div>
  </div>