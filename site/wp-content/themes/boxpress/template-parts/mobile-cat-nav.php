<div class="news-mobile-masthead">
  <div class="mobile-content">
    <div class="news-mobile-header">
      <h2>News</h2>
    </div>
    <div class="news-mobile-close-button">


    <div class="mobile-cat-close">

    </div>
    </div>



  </div>

  <div class="cat-nav">
    <?php
      /**
       * Blog Category Links
       */
      $blog_current_term = '';

      $blog_cats = get_terms( array(
        'taxonomy'    => 'category',
        // 'hide_empty'  => false,
        'exclude'     => '1',
      ));

      $blog_current_term = get_queried_object()->term_id;
    ?>

    <?php if ( $blog_cats && ! is_wp_error( $blog_cats )) : ?>

      <div class="sidebar-widget">
        <nav class="categories-widget">
          <ul>
            <li class="<?php
                if ( is_home() ) {
                  echo 'is-current-cat';
                }
              ?>">
              <a href="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                <svg class="mobile-cat-icon" width="30" height="30">
                  <use xlink:href="#icon-category">
                </svg>
                <?php _e('All Categories', 'boxpress'); ?>
              </a>
            </li>

            <?php foreach ( $blog_cats as $term ) : ?>

              <li class="<?php
                  if ( $blog_current_term === $term->term_id ) {
                    echo 'is-current-cat';
                  }
                ?>">
                <div class="mobile-cat-icon">
                  <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                    <use xlink:href="#icon-<?php echo $term->slug; ?>">
                  </svg>
                </div>
                <a href="<?php echo esc_url( get_term_link( $term )); ?>">
                  <?php echo $term->name; ?>
                </a>
              </li>

            <?php endforeach; ?>

          </ul>
        </nav>
      </div>
    <?php endif; ?>
  </div>
</div>
