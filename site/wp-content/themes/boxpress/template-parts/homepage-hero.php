<?php
/**
 * Displays the Hero layout
 *
 * @package BoxPress
 */
 $hero_subhead           = get_field('hero_subhead');
 $hero_background        = get_field('background');
 //$hero_background_image  = get_field('hero_background_image');

 $hero_link      = get_field('hero_link');
 $hero_link_text = get_field('hero_link_text');

 $attachment_id = get_field('hero_background_image');
 $size = "home_slideshow"; // (thumbnail, medium, large, full or custom size)
 $image = wp_get_attachment_image_src( $attachment_id, $size );

?>

<section class="hero">
  <video class="bg-video vs-source" playsinline autoplay muted loop>
    <source src="<?php bloginfo('template_directory');?>/assets/video/pitt-cba-vid.mp4" type="video/mp4">
  </video>
  <div class="gradient"></div>

  <div class="wrap">

    <div class="circle-container">
      <h2 class="vs" data-vs-in-time="0" data-vs-in-class="spin" data-vs-out-time="5" data-vs-out-class="fadeOut"></h2>
      <h2 class="vs" data-vs-in-time="5" data-vs-in-class="spin-2" data-vs-out-time="9" data-vs-out-class="fadeOut"></h2>
      <h2 class="vs" data-vs-in-time="9" data-vs-in-class="spin-3" data-vs-out-time="15" data-vs-out-class="fadeOut"></h2>
      <div id="circles">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
     </div>
    </div>
    <div class="hero-box desktop">

    </div>
 </div>


  <div class="color-blocks">
        <?php
            $mobile_heading_1 = get_field('mobile_heading_1');
            $mobile_heading_1 = get_field('mobile_heading_2');
            $mobile_heading_1 = get_field('mobile_heading_3');
          ?>
          <div class="color-block-1">
            <h3><?php the_field('mobile_heading_1'); ?></h3>
          </div>
          <div class="color-block-2">
            <h3><?php the_field('mobile_heading_2'); ?></h3>
          </div>
          <div class="color-block-3">
            <h3><?php the_field('mobile_heading_3'); ?></h3>
          </div>
        </div>
    </div>




</section>
