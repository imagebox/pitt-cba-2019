
<div class="sharing">
  <h5><?php _e( 'Share', 'boxpress' ); ?></h5>
  <a class="js-social-share"
     target="_blank"
     href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode('' . the_permalink() . ''); ?>">
    <svg class="social-facebook-svg" width="31" height="31">
      <use xlink:href="#social-facebook"></use>
    </svg>
  </a>
  <a class="js-social-share"
     target="_blank"
     href="https://twitter.com/intent/tweet/?text=<?php the_title(); ?>&url=<?php echo urlencode('' . the_permalink() . ''); ?>">
    <svg class="social-twitter_username-svg" width="31" height="31">
      <use xlink:href="#social-twitter_username"></use>
    </svg>
  </a>
<a href="mailto:?subject=<?php the_title();?>&body=<?php the_permalink();?>">
    <svg class="social-email-svg" width="22" height="14">
      <use xlink:href="#social-email"></use>
    </svg>
  </a>
  <!-- email  -->
  <a class="js-social-share"
     target="_blank"
     href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode('' . the_permalink() . ''); ?>&title=<?php echo urlencode( '' . get_the_title() . '' ); ?>&source=<?php echo urlencode('' . the_permalink() . ''); ?>&summary=<?php echo urlencode( '' . strip_tags( get_the_excerpt() ) . '' ); ?>">
     <svg class="social-linkedin-svg" width="18" height="18">
           <use xlink:href="#social-linkedin"></use>
    </svg>
  </a>
</div>
