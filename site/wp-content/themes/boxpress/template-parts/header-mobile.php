
<div class="site-header--mobile">
  <div class="mobile-header-left">
    <div class="site-branding">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <svg class="site-logo" width="174" height="53">
            <use xlink:href="#pitt-logo-header"></use>
        </svg>
      </a>
    </div>
  </div>
  <div class="mobile-header-right">
    <a href="#" class="menu-button toggle-nav">
      <span class="screen-reader-text"><?php _e('Menu', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="50" height="51">
        <use xlink:href="#menu-icon"></use>
      </svg>
    </a>
  </div>
</div>

<div class="mobile-nav-tray">
  <nav class="navigation--mobile">
    <div class="mobile-nav-header">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <svg class="site-logo" width="174" height="53">
            <use xlink:href="#pitt-logo-header"></use>
        </svg>
      </a>
      <a class="toggle-nav" href="#">
        <svg class="menu-icon-svg" width="50" height="51">
          <use xlink:href="#close-icon"></use>
        </svg>
      </a>


    </div>
<div class="mobile-color-blocks">
  <div></div>
  <div></div>
  <div></div>
</div>
    <?php if ( has_nav_menu( 'secondary' ) ) : ?>
      <ul class="mobile-nav mobile-nav--utility mobile-navigation--secondary">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'secondary',
            'items_wrap'      => '%3$s',
            'container'       => false,
          ));
        ?>
      </ul>

      <?php if ( has_nav_menu( 'button' )) : ?>
      <ul class="nav-list mobile-navigation--button">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'button',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
      <?php endif; ?>

          <?php if ( has_nav_menu( 'primary' ) ) : ?>
            <ul class="mobile-nav mobile-nav--main mobile-navigation--primary">
              <?php
                wp_nav_menu( array(
                  'theme_location'  => 'primary',
                  'items_wrap'      => '%3$s',
                  'container'       => false,
                ));
              ?>
            </ul>
          <?php endif; ?>

    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
           <label>
             <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
             <input type="search" class="search-field"
             placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>"
             value="<?php echo get_search_query() ?>" name="s"
             title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
           </label>
           <input type="submit" class="search-submit"
           value="" />
     </form>

<div class="top-bar">
  <ul class="nav-list">
      <?php
        wp_nav_menu( array(
          'theme_location'  => 'topfull',
          'items_wrap'      => '%3$s',
          'container'       => false,
          'walker'          => new Aria_Walker_Nav_Menu(),
        ));
      ?>
  </ul>
<?php endif; ?>
</div>


<?php get_template_part( 'template-parts/social-nav' ); ?>

    <div class="mobile-color-blocks">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </nav>
</div>
