<?php
/**
 * Displays the staff member template
 */

   $profile_image  = get_field( 'profile_image' );
   $job_title      = get_field( 'job_title' );
   $pitt_contact_phone      = get_field( 'pitt_contact_phone' );
   $pitt_contact_email      = get_field( 'pitt_contact_email' );
   $pitt_contact_twitter      = get_field( 'pitt_contact_twitter' );
   $pitt_contact_more  = get_field( 'pitt_contact_more' );
   $pitt_contact_more_link  = get_field( 'pitt_contact_more_link' );
?>

        <div class="contact-content">

            <div class="contact-image">

              <?php if ( $profile_image ) :
                  $profile_image_url  = $profile_image['sizes'][ 'headshot_image' ];
                  $profile_image_w    = $profile_image['sizes'][ 'headshot_image' . '-width' ];
                  $profile_image_h    = $profile_image['sizes'][ 'headshot_image' . '-height' ];
                ?>

                <img src="<?php echo $profile_image_url; ?>"
                  width="<?php echo $profile_image_w; ?>"
                  height="<?php echo $profile_image_h; ?>"
                  alt="">

                <?php endif; ?>

            </div>

            <div class="text">
              <h3><?php the_title(); ?></h3>
              <p><?php echo $job_title; ?></p>
              <p><?php echo $pitt_contact_phone ?></p>
                <p><a class="no-underline" href="mailto:<?php echo $pitt_contact_email ?>"><?php echo $pitt_contact_email ?></a></p>
                  <p><a class="no-underline" href="<?php echo $pitt_contact_more_link ?>"><?php echo $pitt_contact_more ?></a></p>
              <p><a class="no-underline" href="https://twitter.com/<?php echo $pitt_contact_twitter ?>"><?php echo $pitt_contact_twitter ?></a></p>
            </div>

        </div>
