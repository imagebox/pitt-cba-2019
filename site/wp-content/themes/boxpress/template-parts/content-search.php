<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="search-entry-header">
    <div class="search-content-header">
      <?php if ( has_post_thumbnail() ) : ?>
        <?php the_post_thumbnail('home_index_thumb');?>
      <?php endif; ?>
    </div>
    <div class="search-content-body">
      <?php boxpress_entry_footer(); ?>
      <?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
      <?php if ( 'post' == get_post_type() ) : ?>
      <?php endif; ?>
    </div>
  </header><!-- .entry-header -->

</article><!-- #post-## -->
