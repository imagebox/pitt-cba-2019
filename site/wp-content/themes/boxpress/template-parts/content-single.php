<?php
/**
 * @package BoxPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
    <?php boxpress_entry_footer(); ?>
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

    <?php if ( has_post_thumbnail() ) : ?>
      <?php the_post_thumbnail('home_index_thumb');?>
    <?php endif; ?>

    <div class="entry-meta">
      <?php boxpress_posted_on(); ?>
      <?php include_once('social-share.php'); ?>
    </div>
  </header>

  <?php the_content(); ?>
  <?php
    wp_link_pages( array(
      'before' => '<div class="page-links">' . __( 'Pages:', 'boxpress' ),
      'after'  => '</div>',
    ) );
  ?>

  <footer class="entry-footer">

  </footer>
</article>
