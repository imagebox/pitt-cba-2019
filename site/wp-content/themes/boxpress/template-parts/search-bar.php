<?php
/**
 * Displays the Search Bar
 *
 * @package BoxPress
 */
?>
<div class="search-bar">
  <form method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <label>
      <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
      <input type="search" class="search-field"
          placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>"
          value="<?php echo get_search_query() ?>" name="s"
          title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>">
    </label>
    <button type="submit" class="button search-submit">
      <span class="vh">Search</span>
      <svg class="search-svg" width="15" height="15">
        <use xlink:href="#search-icon"></use>
      </svg>
    </button>
  </form>
</div>
