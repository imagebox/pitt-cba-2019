<?php
/**
 * Displays the search page banner
 *
 * @package BoxPress
 */

$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );

if ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

?>
<header class="banner default-banner">
  <div class="wrap">
    <div class="banner-title">
      <?php if (strpos($_SERVER['REQUEST_URI'], "post_type=post") !== false) { ?>
      	<span class="h1">News Search</span>
      <?php } else if (strpos($_SERVER['REQUEST_URI'], "post_type=careers") !== false) { ?>
      	<span class="h1">Career Blog Search</span>
      <?php } else { ?>
      	<span class="h1">Search</span>
      <?php } ?>
    </div>
    <div class="banner-image">
      <img src="<?php echo $banner_image_url; ?>" alt="">
    </div>
  </div>
</header>
