<?php
/**
 * Displays the Archive page banner
 *
 * @package BoxPress
 */

$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );

if ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

?>
<header class="banner default-banner">
  <div class="wrap">
    <div class="banner-title">
      <span class="h1">
        <?php if ( is_day() ) : ?>
          Archive: <?php echo  get_the_date( 'D M Y' ); ?>
        <?php elseif ( is_month() ) : ?>
          Archive: <?php echo  get_the_date( 'F Y' ); ?>
        <?php elseif ( is_year() ) : ?>
          Archive: <?php echo  get_the_date( 'Y' ); ?>
        <?php else : ?>
          Archive
        <?php endif; ?>
      </span>
    </div>
    <div class="banner-image">
      <img src="<?php echo $banner_image_url; ?>" alt="">
    </div>
  </div>
</header>
