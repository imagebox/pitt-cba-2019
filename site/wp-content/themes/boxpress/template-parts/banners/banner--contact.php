<?php
/**
 * Displays the page banner. Defaults to image set in the options page.
 */

$banner_title     = get_the_title();
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );
$background_banner_image = get_field('background_banner_image');

if ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

// Set top page title and banner image
if ( 0 !== $post->post_parent ) {
  $post_ancestors = get_post_ancestors( $post->ID );
  $post_id = end( $post_ancestors );
  $banner_title = get_the_title( $post_id );
  $top_featured_image = get_the_post_thumbnail_url( $post_id, 'banner_image' );

  if ( ! empty( $top_featured_image )) {
    $banner_image_url = $top_featured_image;
  }
}


if ( has_post_thumbnail() ) {
  $banner_image_url = get_the_post_thumbnail_url( get_the_ID(), 'banner_image' );
}

?>


  <header class="banner" style="background-image: url(<?php echo esc_url($background_banner_image); ?>); background-repeat: no-repeat; background-size: cover;">
  <div class="wrap">
    <div class="banner-title">
        <h2>
        <?php echo $banner_title; ?>
        </h2>
      </span>
    </div>
    <div class="banner-image">
      <img src="<?php echo $banner_image_url; ?>" alt="">
    </div>
  </div>
</header>
