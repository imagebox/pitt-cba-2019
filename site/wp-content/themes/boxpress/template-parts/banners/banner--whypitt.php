<?php
/**
 * Displays the page banner. Defaults to image set in the options page.
 */

$banner_title     = get_the_title();
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );
$background_banner_image = get_field('background_banner_image');
$why_banner_description = get_field('why_banner_description');
$ov_line_color_why = get_field('ov_line_color_why');

if ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

// Set top page title and banner image
if ( 0 !== $post->post_parent ) {
  $post_ancestors = get_post_ancestors( $post->ID );
  $post_id = end( $post_ancestors );
  $banner_title = get_the_title( $post_id );
  $top_featured_image = get_the_post_thumbnail_url( $post_id, 'banner_image' );

  if ( ! empty( $top_featured_image )) {
    $banner_image_url = $top_featured_image;
  }
}


if ( has_post_thumbnail() ) {
  $banner_image_url = get_the_post_thumbnail_url( get_the_ID(), 'banner_image' );
}

?>


  <header class="why-pitt-banner" style="background-image: url(<?php echo esc_url($background_banner_image); ?>); background-repeat: no-repeat; background-size: cover;">

<div class="overview-overlay">

</div>
  <div class="wrap">
    <div class="banner-title why-pitt-banner-title">
        <h2 class="<?php echo $ov_line_color_why; ?>">
        <?php echo $banner_title; ?>
      </h2>

      <p><?php echo $why_banner_description; ?></p>

      </span>
    </div>
    <div class="banner-image">
      <img src="<?php echo $banner_image_url; ?>" alt="">
    </div>
  </div>

      <div class="section-color-blocks">
        <div class="color-block-1">
        </div>
        <div class="color-block-2">
        </div>
        <div class="color-block-3">
        </div>
      </div>


  </header>
  <div class="section-sub-banner">
      <div class="section-sub-banner-content">
        <h2><?php echo $banner_title; ?></h2>
        <p><?php echo $why_banner_description; ?></p>
      </div>
    </div>
