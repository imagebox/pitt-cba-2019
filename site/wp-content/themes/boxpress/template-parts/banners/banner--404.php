<?php
/**
 * Displays the 404 page banner
 *
 * @package BoxPress
 */

$banner_title = '404';
$banner_image_url = '';
$default_banner       = get_field( 'default_banner_image', 'option' );
$four_oh_four_banner  = get_field( 'four_oh_four_banner', 'option' );

if ( $four_oh_four_banner ) {
  $banner_image_url = $four_oh_four_banner['url'];
} elseif ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}

?>
<header class="banner">
  <div class="wrap">
    <div class="banner-title">
      <span class="h1"> 
        <?php echo $banner_title; ?>
      </span>
    </div>
    <?php if ( ! empty( $banner_image_url ) ) : ?>
      <div class="banner-image">
        <img src="<?php echo $banner_image_url; ?>" alt="">
      </div>
    <?php endif; ?>
  </div>
</header>
