<?php
/**
 * Displays an article card
 */



?>

<?php

$careers_posts = get_field('select_career_post');

if( $careers_posts ) {



  ?>



	<?php foreach( $careers_posts as $career_post ): // variable must NOT be called $post (IMPORTANT) ?>
    <?php
    // $card_thumbnail = get_the_post_thumbnail( get_the_ID(), 'card_post_thumb', '' );
    $cp_card_thumbnail = get_the_post_thumbnail( $career_post->ID, 'card_post_thumb' );
    $cp_card_terms = get_the_terms( $career_post->ID, 'careers_categories' );

    $cp_card_term_name = '';
    $cp_card_term_slug = '';

    if ( $cp_card_terms && ! is_wp_error( $cp_card_terms )) {
      foreach ( $cp_card_terms as $term ) {
        $cp_card_term_name = $term->name;
        $cp_card_term_slug = $term->slug;
      }
    }
    ?>

    <div class="card-blog">
      <div class="card-article">
        <a href="<?php echo get_permalink($career_post->ID ); ?>">

          <div class="card-article-header">
            <?php if ( ! empty( $cp_card_thumbnail )) : ?>
              <?php echo $cp_card_thumbnail; ?>
            <?php endif; ?>
          </div>

          <div class="card-article-body">

            <?php if ( ! empty( $cp_card_term_name ) && ! empty( $cp_card_term_slug )) : ?>
                <div class="card-article-icon">
                  <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                    <use xlink:href="#icon-<?php echo $term->slug; ?>">
                  </svg>
                </div>
                <h5 class="card-article-cat"><?php echo $cp_card_term_name; ?></h5>
              <?php endif; ?>

              <h3 class="card-article-title"><?php echo get_the_title( $career_post->ID ); ?></h3>
            </div>
          </a>
        </div>
    </div>
      <!-- <p>
	    	<a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a>
	    	<span>Custom field from $post: <?php the_field('author', $p->ID); ?></span>
	    </p> -->
	<?php endforeach; ?>

<?php } else { ?>

  <?php
  $card_thumbnail = get_the_post_thumbnail( get_the_ID(), 'card_post_thumb', '' );
  $card_terms = get_the_terms( get_the_ID(), 'category' );

  $card_term_name = '';
  $card_term_slug = '';

  if ( $card_terms && ! is_wp_error( $card_terms )) {
    foreach ( $card_terms as $term ) {
      $card_term_name = $term->name;
      $card_term_slug = $term->slug;
    }
  }
  ?>

  <div class="card-blog">
    <div class="card-article">
      <a href="<?php the_permalink(); ?>">

        <div class="card-article-header">
          <?php if ( ! empty( $card_thumbnail )) : ?>
            <?php echo $card_thumbnail; ?>
          <?php endif; ?>
        </div>

        <div class="card-article-body">

          <?php if ( ! empty( $card_term_name ) && ! empty( $card_term_slug )) : ?>
              <div class="card-article-icon">
                <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                  <use xlink:href="#icon-<?php echo $term->slug; ?>">
                </svg>
              </div>
              <h5 class="card-article-cat"><?php echo $card_term_name; ?></h5>
            <?php endif; ?>

            <h3 class="card-article-title"><?php the_title(); ?></h3>
          </div>
        </a>
      </div>
  </div>
<?php } ?>
