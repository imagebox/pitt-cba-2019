<?php
/**
 * Displays an article card
 */



?>

<?php

$news_posts = get_field('select_news_post');

if( $news_posts ) {



  ?>



	<?php foreach( $news_posts as $news_post ): // variable must NOT be called $post (IMPORTANT) ?>
    <?php
    // $card_thumbnail = get_the_post_thumbnail( get_the_ID(), 'card_post_thumb', '' );
    $np_card_thumbnail = get_the_post_thumbnail( $news_post->ID, 'card_post_thumb' );
    $np_card_terms = get_the_terms( $news_post->ID, 'careers_categories' );

    $np_card_term_name = '';
    $np_card_term_slug = '';

    if ( $np_card_terms && ! is_wp_error( $np_card_terms )) {
      foreach ( $np_card_terms as $term ) {
        $np_card_term_name = $term->name;
        $np_card_term_slug = $term->slug;
      }
    }
    ?>

    <div class="card-blog">
      <div class="card-article">
        <a href="<?php echo get_permalink($news_post->ID ); ?>">

          <div class="card-article-header">
            <?php if ( ! empty( $np_card_thumbnail )) : ?>
              <?php echo $np_card_thumbnail; ?>
            <?php endif; ?>
          </div>

          <div class="card-article-body">

            <?php if ( ! empty( $np_card_term_name ) && ! empty( $np_card_term_slug )) : ?>
                <div class="card-article-icon">
                  <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                    <use xlink:href="#icon-<?php echo $term->slug; ?>">
                  </svg>
                </div>
                <h5 class="card-article-cat"><?php echo $np_card_term_name; ?></h5>
              <?php endif; ?>

              <h3 class="card-article-title"><?php echo get_the_title( $news_post->ID ); ?></h3>
            </div>
          </a>
        </div>
    </div>
      <!-- <p>
	    	<a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a>
	    	<span>Custom field from $post: <?php the_field('author', $p->ID); ?></span>
	    </p> -->
	<?php endforeach; ?>
<?php } else { ?>

  <?php
  $card_thumbnail = get_the_post_thumbnail( get_the_ID(), 'card_post_thumb', '' );
  $card_terms = get_the_terms( get_the_ID(), 'careers_categories' );

  $card_term_name = '';
  $card_term_slug = '';

  if ( $card_terms && ! is_wp_error( $card_terms )) {
    foreach ( $card_terms as $term ) {
      $card_term_name = $term->name;
      $card_term_slug = $term->slug;
    }
  }
  ?>

  <div class="card-blog">
    <div class="card-article">
      <a href="<?php the_permalink(); ?>">

        <div class="card-article-header">
          <?php if ( ! empty( $card_thumbnail )) : ?>
            <?php echo $card_thumbnail; ?>
          <?php endif; ?>
        </div>

        <div class="card-article-body">

          <?php if ( ! empty( $card_term_name ) && ! empty( $card_term_slug )) : ?>
              <div class="card-article-icon">
                <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                  <use xlink:href="#icon-<?php echo $term->slug; ?>">
                </svg>
              </div>
              <h5 class="card-article-cat"><?php echo $card_term_name; ?></h5>
            <?php endif; ?>

            <h3 class="card-article-title"><?php the_title(); ?></h3>
          </div>
        </a>
      </div>
  </div>
<?php } ?>
