<?php
/**
 * Displays the sidebar
 *
 * @package BoxPress
 */
?>
<aside class="sidebar" role="complementary">

  <?php // Page ?>
  <?php if ( is_page() ) :
      /**
       * Query for Child Pages
       * ---
       * If we've already queries for the child
       * pages, don't query again.
       */
      if ( ! isset( $child_pages_list )) {
        $child_pages_list = query_for_child_page_list();
      }
    ?>
    <?php if ( $child_pages_list ) : ?>

      <div class="sidebar-widget">
        <nav class="sidebar-nav">
          <ul>

            <?php echo $child_pages_list; ?>

          </ul>
        </nav>
      </div>

    <?php endif; ?>
  <?php endif; ?>


  <?php // Blog Archive ?>
  <?php if ( is_home() ||
             ( is_archive() && get_post_type() == 'post' ) ||
             is_singular( 'post' )) : ?>

    <h1 class="blog-sidebar-title"><?php _e('News', 'boxpress'); ?></h1>

    <?php
      /**
       * Blog Category Links
       */
      $blog_current_term = '';

      $blog_cats = get_terms( array(
        'taxonomy'    => 'category',
        // 'hide_empty'  => false,
        'exclude'     => '1',
      ));

      $blog_current_term = get_queried_object()->term_id;
    ?>

    <?php if ( $blog_cats && ! is_wp_error( $blog_cats )) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Categories', 'boxpress'); ?></h4>
        <nav class="categories-widget">
          <ul>
            <li class="<?php
                if ( is_home() ) {
                  echo 'is-current-cat';
                }
              ?>">
              <a href="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                <svg class="side-bar-nav-cat" width="30" height="30">
                  <use xlink:href="#icon-category">
                </svg>
                <?php _e('All Categories', 'boxpress'); ?>
              </a>
            </li>

            <?php foreach ( $blog_cats as $term ) : ?>

              <li class="<?php
                  if ( $blog_current_term === $term->term_id ) {
                    echo 'is-current-cat';
                  }
                ?>">
                <div class="side-bar-nav-cat">
                  <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                    <use xlink:href="#icon-<?php echo $term->slug; ?>">
                  </svg>
                </div>
                <a href="<?php echo esc_url( get_term_link( $term )); ?>">
                  <?php echo $term->name; ?>
                </a>
              </li>

            <?php endforeach; ?>

          </ul>
        </nav>

      </div>

    <?php endif; ?>


    <?php
      add_filter( 'get_search_form', 'extra_search_form' );
      get_search_form();
      remove_filter( 'get_search_form', 'extra_search_form' );
    ?>


    <?php
        /**
         * Blog Archive Links
         */

        $blog_archives = wp_get_archives( array(
          'type'            => 'monthly',
          'format'          => 'option',
          'echo'            => false,
        ));
      ?>

      <?php if ( $blog_archives ) : ?>

        <div class="sidebar-widget">
          <h4 class="widget-title"><?php _e('Archives', 'boxpress'); ?></h4>
          <div class="archives-widget news-select">
            <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
              <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
              <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                <option value=""><?php echo _e( 'Select Month' ); ?></option>
                <?php echo $blog_archives; ?>
              </select>
            </form>
          </div>

          <div class="popular-post">
            <h4>TRENDING STORIES</h4>
              <?php
                if (function_exists('wpp_get_mostpopular'))
                $args = array(
                'post_html' => '<li>{thumb} {title}</li>',
                'thumbnail_width' => 50,
                'thumbnail_height' => 50,
                'post_type' => 'post',
                'limit' => 4,
                );
                    wpp_get_mostpopular($args);
              ?>
          </div>

        </div>

         </div>

      <?php endif; ?>

  <?php endif; ?>


</aside>
