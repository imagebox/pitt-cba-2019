<?php
/**
 * Displays the post mosaic
 */

$mosaic_index = 1;

?>
<?php if ( have_rows( 'mosaic_tiles' )) : ?>

  <div class="post-content-block post-content-block--mosaic">
    <div class="mosaic-grid">

      <?php while ( have_rows( 'mosaic_tiles' ) ) : the_row();
          $image = get_sub_field( 'mosaic_tile_image' );
          $image_thumb_size = 'mosaic_image';
          $image_caption = wp_get_attachment_caption( $image['id'] );
        ?>
        <?php if ( $image ) : ?>

          <div class="mosaic-grid-item <?php
            // Count every odd item
            if ( $mosaic_index % 2 == 1 ) {
              echo 'mosaic-grid-item--2-1';
            } else {
              echo 'mosaic-grid-item--1-1';
            }
          ?>">
            <div class="mosaic-tile mosaic-tile--image">
              <a class="js-image-gallery-popup" href="<?php echo esc_url( $image['url'] ); ?>" data-caption="<?php echo esc_attr( $image_caption ); ?>">
                <img draggable="false" src="<?php echo esc_url( $image['sizes'][ $image_thumb_size ]); ?>"
                  width="<?php echo esc_attr( $image['sizes'][ $image_thumb_size . '-width' ]); ?>"
                  height="<?php echo esc_attr( $image['sizes'][ $image_thumb_size . '-height' ]); ?>"
                  alt="<?php echo esc_attr( $image['alt'] ); ?>">
              </a>
            </div>
          </div>

        <?php endif; ?>
        
        <?php $mosaic_index++; ?>
      <?php endwhile; ?>

    </div>
  </div>

<?php endif; ?>
