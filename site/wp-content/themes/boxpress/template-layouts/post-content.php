<?php
/**
 * Displays Post Content
 */

$post_content = get_sub_field( 'post_content' );
?>
<?php if ( ! empty( $post_content )) : ?>

  <div class="post-content-block post-content-block--text">
    <?php echo $post_content; ?>
  </div>

<?php endif; ?>
