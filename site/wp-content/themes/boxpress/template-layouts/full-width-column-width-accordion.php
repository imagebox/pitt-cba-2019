<?php
/**
 * Displays the Full Width Column layout
 *
 * @package BoxPress
 */

$background = get_sub_field('background');

// Load sidebar if this is the first template & child pages exist
$is_first_row = ( $row_index == 1 ) ? true : false;

if ( $is_first_row ) {
  $child_pages_list = query_for_child_page_list();
} else {
  // Empty the child list array to prevent false positive
  $child_pages_list = array();
}

?>

<section class="fullwidth-column special-layout advanced-full-width section <?php echo $background; ?>">
  <div class="wrap wrap-accordions <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

    <?php if( !get_sub_field('hide_sidebar') ): ?>
      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
  <?php endif; ?>

      <div class="l-main">

        <?php if ( $is_first_row ) : ?>

          <header class="page-header">

          </header>

        <?php endif; ?>

        <div class="page-content">
          <?php the_sub_field('content'); ?>


          <section class="fullwidth-column <?php echo $background; ?>">
            <div class="wrap">
              <?php $accord_heading = get_sub_field('accord_heading'); ?>
              <?php $heading_text_alignment = get_sub_field('heading_text_alignment'); ?>
              <div class="special-heading <?php echo $heading_text_alignment; ?>">
         <h2><?php echo $accord_heading; ?></h2>
              </div>

              <?php if ( have_rows( 'accordion_row' ) ) : ?>
                <?php while ( have_rows( 'accordion_row' ) ) : the_row(); ?>
                  <?php $accord_title = get_sub_field('accord_title'); ?>
                  <button class="accordion accord-is-open"><?php echo $accord_title; ?>

                    <span>
                      <svg width="45" height="45">
                          <use xlink:href="#accord-button"></use>
                      </svg>
                    </span>
                  </button>
              <div class="accordion-content">
                <p><?php the_sub_field('accord_content'); ?></p>
              </div>

              <?php endwhile; endif; ?>
            </div>
          </section>
          <!-- accordion  -->
        </div>

        <!-- page content  -->
      </div>

      <?php if( !get_sub_field('hide_sidebar') ): ?>

          <?php if ( $child_pages_list ) : ?>
            <div class="l-aside">
              <?php get_sidebar(); ?>
            </div>
          <?php endif; ?>


      <?php endif; ?>

    <!-- Hide Sidebar   -->

  <?php if( !get_sub_field('hide_sidebar') ): ?>
      </div>
  <?php endif; ?>

  </div>
</section>
