<?php
/**
 * Displays the Full Width Column layout w/rss feed
 *
 * @package BoxPress
 */

$background = get_sub_field('background');

// Load sidebar if this is the first template & child pages exist
$is_first_row = ( $row_index == 1 ) ? true : false;

if ( $is_first_row ) {
  $child_pages_list = query_for_child_page_list();
} else {
  // Empty the child list array to prevent false positive
  $child_pages_list = array();
}

?>

<section class="fullwidth-column advanced-full-width section <?php echo $background; ?>">
  <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

    <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
      <div class="l-main">

        <?php if ( $is_first_row ) : ?>

          <header class="page-header">

          </header>

        <?php endif; ?>

        <div class="page-content">
          <?php the_sub_field('content'); ?>


          <?php
          	require_once  (ABSPATH . WPINC . '/class-feed.php');
          	$feed = new SimplePie();
          	$feed->set_feed_url('https://pittbusinesstotheworld.com/feed/');
              $feed->enable_cache(false);
          	$feed->init();
          	$feed->handle_content_type();
          ?>
          <ul>
          <?php foreach ($feed->get_items(0, 3) as $item): ?>
          	<li>
                  <a href="<?php print $item->get_permalink(); ?>" target="_blank">
                      <h2><?php print $item->get_title(); ?></h2>
          			<span class="read-more">Read More</span>
                  </a>
          	</li>
          <?php endforeach; ?>
          </ul>

          <!-- rss feed  -->

          <?php if( get_sub_field('add_a_carousel') ): ?>

            <section>
              <div class="wrap wrap--limited section">

                <?php if ( have_rows( 'carousel_slides' )) : ?>

                  <div class="js-owl-carousel owl-carousel" data-slider-id="1">

                    <?php while ( have_rows( 'carousel_slides' )) : the_row();
                        $carousel_image = get_sub_field('carousel_image');
                      ?>

                      <?php if ( $carousel_image ) :
                          $image_size = 'carousel_image';
                          $carousel_image_url = $carousel_image['sizes'][ $image_size ];
                          $carousel_image_w   = $carousel_image['sizes'][ $image_size . '-width' ];
                          $carousel_image_h   = $carousel_image['sizes'][ $image_size . '-height' ];
                        ?>

                        <div class="carousel-image">
                          <img src="<?php echo $carousel_image_url; ?>"
                            width="<?php echo $carousel_image_w; ?>"
                            height="<?php echo $carousel_image_h; ?>"
                            alt="<?php echo $carousel_image['alt']; ?>">

                          <?php if( $carousel_image['caption'] ): ?>
                          <span class="carousel-caption"> <?php echo $carousel_image['caption']; ?></span>
                          <?php endif; ?>
                        </div>


                      <?php endif; ?>
                    <?php endwhile; ?>

                  </div>


                <?php endif; ?>
                <!-- carousel  -->

                <?php if ( have_rows( 'carousel_slides' )) : ?>

                  <div class="owl-thumbs" data-slider-id="1">

                    <?php while ( have_rows( 'carousel_slides' )) : the_row();
                        $carousel_image = get_sub_field('carousel_image');
                      ?>

                      <?php if ( $carousel_image ) :
                          $image_size = 'carousel_thumb';
                          $carousel_image_url = $carousel_image['sizes'][ $image_size ];
                          $carousel_image_w   = $carousel_image['sizes'][ $image_size . '-width' ];
                          $carousel_image_h   = $carousel_image['sizes'][ $image_size . '-height' ];
                        ?>


                          <img class="owl-thumb-item" src="<?php echo $carousel_image_url; ?>" alt="">

                        <!-- carousel thumbs -->


                      <?php endif; ?>
                    <?php endwhile; ?>

                  </div>


                <?php endif; ?>

              </div>
            </section>


          <?php endif; ?>
        </div>
      </div>

      <?php if ( $child_pages_list ) : ?>
        <div class="l-aside">
          <?php get_sidebar(); ?>
        </div>
      <?php endif; ?>
    </div>

  </div>
</section>
