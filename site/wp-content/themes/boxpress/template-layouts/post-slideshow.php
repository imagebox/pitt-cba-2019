
<?php

// Display carousel block

$background = get_sub_field( 'background' );

 ?>


<section class="post-content-block">
  <div class="wrap wrap--limited owl-wrap section sidebar-layout-block news-slider-handle post-content-block--slider <?php echo $background; ?>">
    <?php $carousel_heading = get_sub_field('carousel_heading'); ?>
    <?php $heading_text_alignment = get_sub_field('heading_text_alignment'); ?>
    <?php $heading_text_alignment = get_sub_field('heading_text_alignment'); ?>
    <div class="special-heading <?php echo $heading_text_alignment; ?> ">

        <?php  if ( ! empty( $carousel_heading ) ) : ?>
          <h2><?php echo $carousel_heading; ?></h2>
        <?php endif; ?>

    </div>
    <?php if ( have_rows( 'post_slideshow_slides' )) : ?>

      <div class="js-owl-carousel owl-carousel" data-slider-id="slider-<?php echo $row_index;?>">

        <?php while ( have_rows( 'post_slideshow_slides' )) : the_row();
            $carousel_image = get_sub_field('carousel_image');
          ?>

          <?php if ( $carousel_image ) :
              $carousel_image_url = $carousel_image['sizes'][ 'carousel_slides_size' ];
              $carousel_image_w   = $carousel_image['sizes'][ 'carousel_slides_size' . '-width' ];
              $carousel_image_h   = $carousel_image['sizes'][ 'carousel_slides_size' . '-height' ];
            ?>

            <div class="carousel-image">
              <img src="<?php echo $carousel_image_url; ?>"
                width="<?php echo $carousel_image_w; ?>"
                height="<?php echo $carousel_image_h; ?>"
                alt="<?php echo $carousel_image['alt']; ?>">
                <?php if( $carousel_image['caption'] ): ?>
                <span class="carousel-caption"> <?php echo $carousel_image['caption']; ?></span>
                <?php endif; ?>
            </div>

          <?php endif; ?>
        <?php endwhile; ?>

      </div>


    <?php endif; ?>
    <!-- carousel  -->



  </div>
</section>
