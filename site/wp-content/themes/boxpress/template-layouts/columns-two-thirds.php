<?php
/**
 * Displays the Two Third Col layout
 */

$one_third_position = get_sub_field( 'one_third_column_position' );
$two_thirds_content = get_sub_field( 'two_thirds_content' );
$one_third_content  = get_sub_field( 'one_third_content' );
$background = get_sub_field( 'background' );
$left_border = get_sub_field('left_border');

?>


    <section class="fullwidth-column section <?php echo $background; ?>">

      <div class="wrap">

        <div class="columns-two-thirds columns-two-thirds--<?php echo $one_third_position; ?>">
          <div class="col-two-third <?php echo $background; ?> <?php
              if ( $left_border ) {
                echo 'block-border';
              }
            ?>">
            <div class="page-content">
              <?php echo $two_thirds_content; ?>

            </div>
          </div>
          <div class="col-one-third">
            <div class="page-content">
              <?php echo $one_third_content; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
