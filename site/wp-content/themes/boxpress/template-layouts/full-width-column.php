<?php
/**
 * Displays the Full Width Column layout
 *
 * @package BoxPress
 */

$background = get_sub_field('background');

// Load sidebar if this is the first template & child pages exist
$is_first_row = ( $row_index == 1 ) ? true : false;

if ( $is_first_row ) {
  $child_pages_list = query_for_child_page_list();
} else {
  // Empty the child list array to prevent false positive
  $child_pages_list = array();
}

?>

<section class="fullwidth-column advanced-full-width section <?php echo $background; ?>">
  <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

    <?php if( !get_sub_field('hide_sidebar') ): ?>
      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
  <?php endif; ?>

      <div class="l-main">

        <?php if ( $is_first_row ) : ?>

          <header class="page-header">

          </header>

        <?php endif; ?>

        <div class="page-content">
          <?php the_sub_field('content'); ?>
        </div>
      </div>

      <?php if( !get_sub_field('hide_sidebar') ): ?>

          <?php if ( $child_pages_list ) : ?>
            <div class="l-aside">
              <?php get_sidebar(); ?>
            </div>
          <?php endif; ?>


      <?php endif; ?>

    <!-- Hide Sidebar   -->

  <?php if( !get_sub_field('hide_sidebar') ): ?>
      </div>
  <?php endif; ?>

  </div>
</section>
