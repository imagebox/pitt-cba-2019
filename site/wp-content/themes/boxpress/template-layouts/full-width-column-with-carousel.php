<?php
/**
 * Displays the Full Width Column layout
 *
 * @package BoxPress
 */

$background = get_sub_field('background');

// Load sidebar if this is the first template & child pages exist
$is_first_row = ( $row_index == 1 ) ? true : false;

if ( $is_first_row ) {
  $child_pages_list = query_for_child_page_list();
} else {
  // Empty the child list array to prevent false positive
  $child_pages_list = array();
}

?>



<section class="fullwidth-column advanced-full-width section <?php echo $background; ?>">
  <div class="wrap <?php if ( ! $child_pages_list ) { echo 'wrap--limited'; } ?>">

    <?php if( !get_sub_field('hide_sidebar') ): ?>
      <div class="<?php if ( $child_pages_list ) { echo 'l-sidebar'; } ?>">
  <?php endif; ?>

      <div class="l-main">

        <?php if ( $is_first_row ) : ?>

          <header class="page-header">

          </header>

        <?php endif; ?>

        <div class="page-content">
          <?php the_sub_field('content'); ?>


          <section>
            <div class="wrap wrap--limited owl-wrap section">
              <?php $carousel_heading = get_sub_field('carousel_heading'); ?>
              <?php $heading_text_alignment = get_sub_field('heading_text_alignment'); ?>
              <div class="special-heading <?php echo $heading_text_alignment; ?>">
          <h2><?php echo $carousel_heading; ?></h2>
              </div>
              <?php if ( have_rows( 'carousel_slides' )) : ?>

                <div class="js-owl-carousel owl-carousel" data-slider-id="slider-<?php echo $row_index;?>">

                  <?php while ( have_rows( 'carousel_slides' )) : the_row();
                      $carousel_image = get_sub_field('carousel_image');
                    ?>

                    <?php if ( $carousel_image ) :
                        $carousel_image_url = $carousel_image['sizes'][ 'carousel_slides_size' ];
                        $carousel_image_w   = $carousel_image['sizes'][ 'carousel_slides_size' . '-width' ];
                        $carousel_image_h   = $carousel_image['sizes'][ 'carousel_slides_size' . '-height' ];
                      ?>

                      <div class="carousel-image">
                        <img src="<?php echo $carousel_image_url; ?>"
                          width="<?php echo $carousel_image_w; ?>"
                          height="<?php echo $carousel_image_h; ?>"
                          alt="<?php echo $carousel_image['alt']; ?>">

                          <?php if( $carousel_image['caption'] ): ?>
                          <span class="carousel-caption"> <?php echo $carousel_image['caption']; ?></span>
                          <?php endif; ?>
                      </div>

                    <?php endif; ?>
                  <?php endwhile; ?>

                </div>


              <?php endif; ?>
              <!-- carousel  -->

              <?php if ( have_rows( 'carousel_slides' )) : ?>

                <div class="owl-thumbs" data-slider-id="slider-<?php echo $row_index;?>">

                  <?php while ( have_rows( 'carousel_slides' )) : the_row();
                      $carousel_image = get_sub_field('carousel_image');
                    ?>

                    <?php if ( $carousel_image ) :
                        $carousel_image_url = $carousel_image['sizes'][ 'carousel_thumb' ];
                        $carousel_image_w   = $carousel_image['sizes'][ 'carousel_thumb' . '-width' ];
                        $carousel_image_h   = $carousel_image['sizes'][ 'carousel_thumb' . '-height' ];
                      ?>


                        <img class="owl-thumb-item" src="<?php echo $carousel_image_url; ?>" alt="">

                      <!-- carousel thumbs -->


                    <?php endif; ?>
                  <?php endwhile; ?>

                </div>


              <?php endif; ?>

            </div>
          </section>
        </div>

        <!-- page content  -->
      </div>

      <?php if( !get_sub_field('hide_sidebar') ): ?>

          <?php if ( $child_pages_list ) : ?>
            <div class="l-aside">
              <?php get_sidebar(); ?>
            </div>
          <?php endif; ?>


      <?php endif; ?>

    <!-- Hide Sidebar   -->

  <?php if( !get_sub_field('hide_sidebar') ): ?>
      </div>
  <?php endif; ?>

  </div>
</section>
