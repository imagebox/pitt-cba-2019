<section class="section">
  <div class="wrap">
    <div class="tabs">
      <?php if( have_rows('tabs') ): $i = 0; ?>
        <?php while( have_rows('tabs') ): the_row(); $i++;
          $tab_label = get_sub_field('tab_label');
        ?>
        <button onclick="openMe('tab-<?php echo $i; ?>')" class="tab"><?php echo $tab_label; ?></button>
         <?php endwhile; ?>
         <?php endif; ?>
      </div>


       <div class="tab-contents">
  		 <?php if( have_rows('tabs') ): $i = 0; ?>
           <?php while( have_rows('tabs') ): the_row(); $i++;
               $tab_content = get_sub_field('tab_content');
           ?>

           <div id="tab-<?php echo $i; ?>" class="content">
              <?php echo $tab_content;?>
           </div>

         <?php endwhile; ?>
         <?php endif; ?>
       </div>
  </div>

</section>
