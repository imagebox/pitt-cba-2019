<?php
/**
 * Displays the Accordion Layout
 *
 * @package BoxPress
 */

$background = get_sub_field( 'background' );
$accord_heading = get_sub_field('accord_heading');
$heading_text_alignment = get_sub_field('heading_text_alignment');

?>


<section class="fullwidth-column special-layout section <?php echo $background; ?>">
  <div class="wrap">
    <div class="special-heading <?php echo $heading_text_alignment; ?>">
 <h2><?php echo $accord_heading ?></h2>
    </div>

    <?php if ( have_rows( 'accordion_row' ) ) : ?>
      <?php while ( have_rows( 'accordion_row' ) ) : the_row(); ?>
    <button class="accordion accord-is-open"></button>
    <div class="accordion-content">
      <p><?php the_sub_field('accord_content'); ?></p>
    </div>

    <span>
      <svg class="site-logo" width="45" height="45">
          <use xlink:href="#accord-button"></use>
      </svg>
    </span>

    <?php endwhile; endif; ?>
  </div>
</section>
