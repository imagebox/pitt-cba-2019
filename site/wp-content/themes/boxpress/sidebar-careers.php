<?php
/**
 * Displays the sidebar
 *
 * @package BoxPress
 */
?>
<aside class="sidebar" role="complementary">


    <h1 class="blog-sidebar-title"><?php _e('Career Blog', 'boxpress'); ?></h1>

    <?php
      /**
       * Blog Category Links
       */
      $blog_current_term = '';

      $blog_cats = get_terms( array(
        'taxonomy'    => 'careers_categories',
        // 'hide_empty'  => false,
        'exclude'     => '1',
      ));

      $blog_current_term = get_queried_object()->term_id;
    ?>

    <?php if ( $blog_cats && ! is_wp_error( $blog_cats )) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Categories', 'boxpress'); ?></h4>
        <nav class="categories-widget">
          <ul>
            <li class="<?php
                if ( is_home() ) {
                  echo 'is-current-cat';
                }
              ?>">
              <a href="/blog/">
                <svg class="side-bar-nav-cat" width="30" height="30">
                  <use xlink:href="#icon-category">
                </svg>
                <?php _e('All Categories', 'boxpress'); ?>
              </a>
            </li>

            <?php foreach ( $blog_cats as $term ) : ?>

              <li class="<?php
                  if ( $blog_current_term === $term->term_id ) {
                    echo 'is-current-cat';
                  }
                ?>">
                <div class="side-bar-nav-cat">
                  <svg class="icon-<?php echo $term->slug; ?>" width="30" height="30">
                    <use xlink:href="#icon-<?php echo $term->slug; ?>">
                  </svg>
                </div>
                <a href="<?php echo esc_url( get_term_link( $term )); ?>">
                  <?php echo $term->name; ?>
                </a>
              </li>

            <?php endforeach; ?>

          </ul>
        </nav>
      </div>

    <?php endif; ?>


    <?php
      add_filter( 'get_search_form', 'extra_search_form_careers' );
      get_search_form();
      remove_filter( 'get_search_form', 'extra_search_form_careers' );
    ?>


    <?php
  /**
   * Blog Archive Links
   */

   $careers_archive = array(
       'post_type'    => 'careers',
       'type'         => 'monthly',
       'format'          => 'option',
       'echo'         => false
   );
?>

<?php if ( $careers_archive ) : ?>

  <div class="sidebar-widget">
    <h4 class="widget-title"><?php _e('Archives', 'boxpress'); ?></h4>
    <div class="archives-widget news-select">
      <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
        <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
        <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
          <option value=""><?php echo _e( 'Select Month' ); ?></option>
          <?php echo wp_get_archives($careers_archive); ?>
        </select>
      </form>
    </div>
  </div>
      <?php endif; ?>









</aside>



<!--

span.wpp-meta.post-stats {
displat: none;
}

.wpp-list li:first-of-type:before{
content: none;
display: none;
}
.wpp-list{
padding-left: none
}

 -->
