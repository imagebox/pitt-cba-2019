<?php
/**
 * Template Name: Tour Overview
 *
 * Displays the page template
 */

get_header(); ?>

    <?php get_template_part('template-parts/banners/banner--tour'); ?>


          <section class="fullwidth-column admission-ov section">
            <div class="wrap">



      <?php if( have_rows('overview_cards') ): ?>

      <div class="l-grid l-grid--four-col l-grid-center">


            	<?php while( have_rows('overview_cards') ): the_row();


          		$card_image_thumb = get_sub_field('card_image_thumb');
              $card_title = get_sub_field('card_title');
          		$card_content = get_sub_field('card_content');
          		$card_link = get_sub_field('card_link');
              $card_hover_color = get_sub_field('card_hover_color');

          		?>


                      <div class="l-grid-item">
                          <div class="card__header">
                            <?php if ( $card_image_thumb ) :
                                $card_image_thumb_url  = $card_image_thumb['sizes'][ 'card_thumb' ];
                                $card_image_thumb_w    = $card_image_thumb['sizes'][ 'card_thumb' . '-width' ];
                                $card_image_thumb_h    = $card_image_thumb['sizes'][ 'card_thumb' . '-height' ];
                              ?>

                              <img src="<?php echo $card_image_thumb_url; ?>"
                                width="<?php echo $card_image_thumb_w; ?>"
                                height="<?php echo $card_image_thumb_h; ?>"
                                alt="">
                              <?php endif; ?>
                          </div>
                          <!-- thumbnail -->

                          <div class="card__body <?php echo $card_hover_color; ?>">
                            <div class="card_content">
                              <?php echo $card_content; ?>

                          <a class="learn-more-button" href="<?php echo $card_link['url']; ?>" target="<?php echo $card_link['target']; ?>"><?php echo $card_link['title']; ?></a>
                            </div>

                          </div>

                      </div>


          	<?php endwhile; ?>
          </div>
        </div>
      </section>
          <?php endif; ?>


      <!-- admission cards -->


      <section>
        <?php

            $tour_icon_1 = get_field('tour_icon_1');
            $tour_icon_2 = get_field('tour_icon_2');
            $tour_icon_3 = get_field('tour_icon_3');
            $tour_heading_1 = get_field('tour_heading_1');
            $tour_heading_2 = get_field('tour_heading_2');
            $tour_heading_3 = get_field('tour_heading_3');
            $tour_description_1 = get_field('tour_description_1');
            $tour_description_2 = get_field('tour_description_2');
            $tour_description_3 = get_field('tour_description_3');
            $tour_pitt_col_link_1 = get_field('tour_pitt_col_link_1');
            $tour_pitt_col_link_2 = get_field('tour_pitt_col_link_2');
            $tour_pitt_col_link_3 = get_field('tour_pitt_col_link_3');
            $tour_pitt_col_link_text_1 = get_field('tour_pitt_col_link_text_1');
            $tour_pitt_col_link_text_2 = get_field('tour_pitt_col_link_text_2');
            $tour_pitt_col_link_text_3 = get_field('tour_pitt_col_link_text_3');

         ?>

          <div class="wrap">
            <div class="l-grid l-grid--three-col tour-icons tour-grid">
                  <div class="l-grid-item">
                    <img src="<?php echo $tour_icon_1; ?>" alt="">
                    <h2><?php echo $tour_heading_1; ?></h2>
                    <p><?php echo $tour_description_1; ?></p>
                    <a class="button" href="<?php echo $tour_pitt_col_link_1; ?>"><?php echo $tour_pitt_col_link_text_1; ?></a>
                  </div>
                  <div class="l-grid-item">
                    <img src="<?php echo $tour_icon_2; ?>" alt="">
                    <h2><?php echo $tour_heading_2; ?></h2>
                    <p><?php echo $tour_description_2; ?></p>
                    <a class="button" href="<?php echo $tour_pitt_col_link_2; ?>"><?php echo $tour_pitt_col_link_text_2; ?></a>
                  </div>
                  <div class="l-grid-item">
                    <img src="<?php echo $tour_icon_3; ?>" alt="">
                    <h2><?php echo $tour_heading_3; ?></h2>
                    <p><?php echo $tour_description_3; ?></p>
                    <a class="button" href="<?php echo $tour_pitt_col_link_3; ?>"><?php echo $tour_pitt_col_link_text_3; ?></a>
                  </div>
              </div>
          </div>
      </section>



<?php get_footer(); ?>
