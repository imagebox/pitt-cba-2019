<?php
/**
 * Displays the Full Width Column layout
 *
 * @package BoxPress
 */

$background = get_sub_field('background');


?>

<section class="fullwidth-column advanced-full-width section sidebar-layout-block <?php echo $background; ?>">
      <div class="page-content">
        <?php the_sub_field('content'); ?>
      </div>
</section>
