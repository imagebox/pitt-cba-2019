<?php
/**
 * Displays the Accordion Layout
 *
 * @package BoxPress
 */

$background = get_sub_field( 'background' );



?>


<div class="table-scroll">
  <div class="wrap">
    <table>
      <tr>
        <th>Lorem Ipsum Dolor</th>
        <th>Consectetur Adipiscing/th>
        <th>Mauris</th>
        <th>Aliquam a Convallis</th>
        <th>Finibus</th>
      </tr>

      <tr>
        <td>Vestibulum imperdiet bibendum risus</td>
        <td>Praesent eget accumsan dolor</td>
        <td>24</td>
        <td>Vivamus vehicula finibus sem, sit amet euismod ante</td>
        <td>05/20/2017</td>
      </tr>

      <tr>
        <td>Vivamus vehicula finibus sem</td>
        <td>Quisque scelerisque mauris nec bibendum</td>
        <td>26</td>
        <td>Cras odio urna, vulputate et orci ac</td>
        <td>05/20/2017</td>
      </tr>
      <tr>
        <td>Praesent mollis justo elementum</td>
        <td>Nullam lacinia velit ac nisi vulputate volutpat</td>
        <td>28</td>
        <td>Pellentesque pharetra sed urna non luctus</td>
        <td>05/20/2017</td>
      </tr>

      <tr>
        <td>Suspendisse laoreet consequat sem nec</td>
        <td>Donec vestibulum lectus in suscipit aliquet</td>
        <td>30</td>
        <td>In sed augue a arcu molestie sodales</td>
        <td>05/20/2017</td>
      </tr>

      <tr>
        <td>Suspendisse laoreet consequat sem nec</td>
        <td>Donec vestibulum lectus in suscipit aliquet</td>
        <td>30</td>
        <td>In sed augue a arcu molestie sodales</td>
        <td>05/20/2017</td>
      </tr>

    </table>
  </div>
</div>
