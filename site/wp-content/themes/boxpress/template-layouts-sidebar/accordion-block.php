<?php
/**
 * Displays the Accordion Layout
 *
 * @package BoxPress
 */

$background = get_sub_field( 'background' );
$accord_heading = get_sub_field('accord_heading');
$heading_text_alignment = get_sub_field('heading_text_alignment');

?>


<section class="section sidebar-layout-block <?php echo $background; ?>">
    <div class="special-heading <?php echo $heading_text_alignment; ?>">

      <?php  if ( ! empty( $accord_heading ) ) : ?>
       <h2><?php echo $accord_heading; ?></h2>
      <?php endif; ?>

    </div>

    <?php if ( have_rows( 'accordion_row' ) ) : ?>
      <?php while ( have_rows( 'accordion_row' ) ) : the_row(); ?>
        <?php $accord_title = get_sub_field('accord_title'); ?>
    <button class="accordion accord-is-open"><?php echo $accord_title; ?>

      <span>
        <svg width="45" height="45">
            <use xlink:href="#accord-button"></use>
        </svg>
      </span>
    </button>
    <div class="accordion-content">
      <p><?php the_sub_field('accord_content'); ?></p>
    </div>

    <?php endwhile; endif; ?>
</section>
