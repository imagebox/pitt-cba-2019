<?php
/**
 * Displays the Full Width Column layout
 *
 * @package BoxPress
 */

  $logo_heading_pitt = get_sub_field('logo_heading_pitt');

?>

<?php if( have_rows('logo_block') ): ?>
  <section class="logo-block fullwidth-column sidebar-layout-block section">
    <div class="wrap">
       <h2><?php echo $logo_heading_pitt; ?></h2>
      <div class="logo-item-block">
        <?php while( have_rows('logo_block') ): the_row();
          // vars
          $logo_block_media = get_sub_field('logo_block_media');
          ?>


          <?php if ( $logo_block_media ) :
              $logo_block_media_url  = $logo_block_media['sizes'][ 'logo_block_media' ];
              $logo_block_media_w    = $logo_block_media['sizes'][ 'logo_block_media' . '-width' ];
              $logo_block_media_h    = $logo_block_media['sizes'][ 'logo_block_media' . '-height' ];
            ?>

            <img class="bkg-image" src="<?php echo $logo_block_media_url; ?>"
              width="<?php echo  $logo_block_media_w; ?>"
              height="<?php echo $logo_block_media_h; ?>"
              alt="">
          <?php endif; ?>
        <?php endwhile; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
