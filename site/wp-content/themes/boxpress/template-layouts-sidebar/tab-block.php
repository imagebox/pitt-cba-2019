  <?php

  $background = get_sub_field( 'background' );
  $tab_heading = get_sub_field('tab_heading');
  $heading_text_alignment = get_sub_field('heading_text_alignment');

  ?>

<section class="section sidebar-layout-block <?php echo $background; ?>">
  <div class="special-heading  <?php echo $heading_text_alignment; ?>">

  <?php  if ( ! empty( $tab_heading ) ) : ?>
    <h2><?php echo $tab_heading; ?></h2>
  <?php endif; ?>
  </div>

    <div class="tabs">
      <?php if( have_rows('tabs') ): $i = 0; ?>
        <?php while( have_rows('tabs') ): the_row(); $i++;
          $tab_label = get_sub_field('tab_label');
        ?>
        <button onclick="openMe('tab-<?php echo $i; ?>')" class="tab"><?php echo $tab_label; ?></button>
         <?php endwhile; ?>
         <?php endif; ?>
      </div>


       <div class="tab-contents">
  		 <?php if( have_rows('tabs') ): $i = 0; ?>
           <?php while( have_rows('tabs') ): the_row(); $i++;
               $tab_content = get_sub_field('tab_content');
           ?>

           <div id="tab-<?php echo $i; ?>" class="content">
              <?php echo $tab_content;?>
           </div>

         <?php endwhile; ?>
         <?php endif; ?>
       </div>
</section>
