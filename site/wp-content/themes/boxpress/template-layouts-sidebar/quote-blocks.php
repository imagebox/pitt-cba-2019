<?php
/**
 * Displays the Two Third Col layout
 */

$two_thirds_content = get_sub_field( 'two_thirds_content' );
$one_third_content  = get_sub_field( 'one_third_content' );
$background = get_sub_field( 'background' );
$circle_image_border = get_sub_field('circle_image_border');
$quote_border = get_sub_field('quote_border');
$quote_line = get_sub_field('quote_line');
$quote_name = get_sub_field('quote_name');
$quote_title = get_sub_field('quote_title');


?>


    <section class="fullwidth-column why-button-block section sidebar-layout-block <?php echo $background; ?>">

        <div class="columns-two-thirds quote-block">
          <div class="col-two-third">
            <div class="page-content quote-border <?php echo $quote_line; ?>">
              <?php echo $two_thirds_content; ?>

              <?php  if ( ! empty( $quote_name ) ) : ?>
              <p class="quote-name"><?php echo $quote_name; ?></p>
            <?php endif; ?>

            <?php if (! empty( $quote_title ) ) : ?>
              <p class="quote-title"><?php echo $quote_title; ?></p>
            <?php endif; ?>
            </div>
          </div>
          <div class="col-one-third">
            <div class="page-content">

    <?php $quote_image = get_sub_field('quote_image'); ?>


        <?php if ( $quote_image ) :
            $quote_image_url  = $quote_image['sizes'][ 'quote_image' ];
            $quote_image_w    = $quote_image['sizes'][ 'quote_image' . '-width' ];
            $quote_image_h    = $quote_image['sizes'][ 'quote_image' . '-height' ];
          ?>

<div class="quote-circle <?php echo $circle_image_border; ?>" style="background-image: url(<?php echo esc_url($quote_image_url); ?>); background-repeat: no-repeat; background-position: center center; width='<?php echo  $quote_image_w; ?>' height='<?php echo $quote_image_h; ?>' ">


    <?php endif; ?>


    <div class="quote-inner-circle <?php echo $quote_border; ?>">
      <img src="<?php bloginfo('template_directory'); ?>/assets/img/quote-circle.png" />
    </div>
</div>


            </div>
          </div>
        </div>
    </section>
