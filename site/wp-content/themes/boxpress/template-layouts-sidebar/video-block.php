<?php

$video_block_media = get_sub_field('video_block_media');
$video_block_title = get_sub_field('video_block_title');
$video_block_body = get_sub_field('video_block_body');
$video_block_button_link = get_sub_field('video_block_button_link');
$video_block_link = get_sub_field('video_block_link');
$background = get_sub_field('background');



 ?>


<section class="section video-block-layout sidebar-layout-block <?php echo $background; ?>">
  <div class="wrap">
    <div class="video-block-l">
      <a class="video-button" href="<?php the_sub_field('video_block_link');?>">


                  <?php if ( $video_block_media ) :
                      $video_block_media_url  = $video_block_media['sizes'][ 'video_block_media' ];
                      $video_block_media_w    = $video_block_media['sizes'][ 'video_block_media' . '-width' ];
                      $video_block_media_h    = $video_block_media['sizes'][ 'video_block_media' . '-height' ];
                    ?>

                    <img class="play-button" src="<?php bloginfo('template_directory'); ?>/assets/img/play.png" />
                    <img class="bkg-image" src="<?php echo $video_block_media_url; ?>"
                      width="<?php echo  $video_block_media_w; ?>"
                      height="<?php echo $video_block_media_h; ?>"
                      alt="">

                <?php endif; ?>
                </a>
    </div>
    <div class="video-block-callout">
      <div class="button-block-copy button-block-copy-why">

          <h2><?php the_sub_field('video_block_title') ?></h2>

        <div class="button-block-body">
            <?php the_sub_field('button_block_body'); ?>

          <?php

          $button_block_link = get_sub_field('button_block_link');

          if( $button_block_link ): ?>

            <a class="button" href="<?php echo $button_block_link['url']; ?>" target="<?php echo $button_block_link['target']; ?>"><?php echo $button_block_link['title']; ?></a>

          <?php endif; ?>
        </div>
      </div>
    </div>
    <!-- video callout  -->
  </div>
</section>
