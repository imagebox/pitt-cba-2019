<?php
/**
 * Displays the Column layout
 *
 * @package BoxPress
 */

$col_heading  = get_sub_field( 'col_heading' );
$background       = get_sub_field('background');
$heading_text_alignment = get_sub_field('heading_text_alignment');
$vertical_line = get_sub_field('vertical_line');

?>
<section class="section sidebar-layout-block <?php echo $background; ?> <?php echo $vertical_line; ?>">


    <?php if ( ! empty( $col_heading )) : ?>
      <div class="special-heading <?php echo $heading_text_alignment; ?>">
        <?php  if ( ! empty( $col_heading ) ) : ?>
          <h2><?php echo $col_heading; ?></h2>
        <?php endif; ?>
      </div>
    <?php endif; ?>

    <?php if ( have_rows( 'column_row' ) ) : ?>
      <?php while ( have_rows( 'column_row' ) ) : the_row();
          $total_cols = get_sub_field( 'number_of_columns' );
        ?>

        <div class="l-columns l-columns--<?php echo $total_cols; ?> ">

          <?php for ( $i = 1; $i <= $total_cols; $i++ ) :
              $column_content = get_sub_field( 'column_' . $i );
            ?>

            <div class="l-column-item ">
              <div class="page-content">

                <?php echo $column_content; ?>

              </div>
            </div>

          <?php endfor; ?>

        </div>

      <?php endwhile; ?>
    <?php endif; ?>


</section>
