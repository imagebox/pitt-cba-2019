<?php
/**
 * Template Name: Admission Overview
 *
 * Displays the page template
 */

get_header(); ?>

    <?php get_template_part('template-parts/banners/banner--admission-ov'); ?>


          <section class="fullwidth-column admission-ov section">
            <div class="wrap">

          <?php if(get_field('admission_ov_intro_heading') || get_field('admission_ov_intro_text')) { ?>

          <div class="admission-ov-intro">
            <h2><?php the_field('admission_ov_intro_heading'); ?></h2>
            <p><?php the_field('admission_ov_intro_text'); ?></p>
          </div>
          <?php } ?>

          </div>
          </section>

          <?php if( have_rows('overview_cards') ): ?>

          	<?php while( have_rows('overview_cards') ): the_row();


          		$card_image_thumb = get_sub_field('card_image_thumb');
              $card_title = get_sub_field('card_title');
          		$card_content = get_sub_field('card_content');
          		$card_link = get_sub_field('card_link');

          		?>

              <div class="l-grid l-grid--four-col l-grid-center">
                      <div class="l-grid-item">
                          <div class="card__header">
                            <?php if ( $card_image_thumb ) :
                                $card_image_thumb_url  = $card_image_thumb['sizes'][ 'card_thumb' ];
                                $card_image_thumb_w    = $card_image_thumb['sizes'][ 'card_thumb' . '-width' ];
                                $card_image_thumb_h    = $card_image_thumb['sizes'][ 'card_thumb' . '-height' ];
                              ?>

                              <img src="<?php echo $card_image_thumb_url; ?>"
                                width="<?php echo $card_image_thumb_w; ?>"
                                height="<?php echo $card_image_thumb_h; ?>"
                                alt="">
                              <?php endif; ?>
                          </div>
                          <!-- thumbnail -->

                          <div class="card__body">
                            <h2><?php echo $card_title ?></h2>
                            <p><?php echo wp_trim_words( get_sub_field('card_content'), 40, '...' ); ?></p>
                            <a class="learn-more-button" href="<?php echo get_the_permalink(); ?>">learn more</a>
                          </div>

                      </div>
                  </div>
                </div>

          	<?php endwhile; ?>
          <?php endif; ?>


      <!-- admission cards -->



  <?php if(get_field('button_block_body_admission_ov') || get_field('button_block_title_admission_ov') || get_field('button_block_text_admission_ov')) { ?>

      <section class="home-section button-block-media-section-admission-ov">
        <div class="wrap">
          <div class="button-block-media">
              <div class="button-block-copy button-block-copy-why-ov">
                    <div class="button-block-header">
                      <h2><?php the_field('button_block_title_admission_ov'); ?></h2>
                        </div>
                        <div class="button-block-body">
                          <p><?php the_field('button_block_body_admission_ov'); ?></p>
                          <a class="button" href="<?php the_field('button_block_link_admission_ov'); ?>"><?php the_field('button_block_text_admission_ov'); ?></a>
                          </div>
                        </div>

                        <?php  $background_image_block_admission_ov  = get_field( 'background_image_block_admission_ov' ); ?>



            <?php if ( $background_image_block_admission_ov ) :
                $background_image_block_admission_ov_url  = $background_image_block_admission_ov['sizes'][ 'background_image_block_admission_ov' ];
                $background_image_block_admission_ov_w    = $background_image_block_admission_ov['sizes'][ 'background_image_block_admission_ov' . '-width' ];
                $background_image_block_admission_ov_h    = $background_image_block_admission_ov['sizes'][ 'background_image_block_admission_ov' . '-height' ];
              ?>
                <img class="media-block bkg-image" src="<?php echo $background_image_block_admission_ov_url; ?>"
                  width="<?php echo  $background_image_block_admission_ov_w; ?>"
                  height="<?php echo $background_image_block_admission_ov_h; ?>"
                  alt="">
            <?php endif; ?>
            <!-- photo-->

          </div>
          </div>
      </section>
        <!-- end pitt image block -->

    <?php } ?>


        <?php if(get_field('admission_ov_icon_1') || get_field('admission_ov_icon_2') || get_field('admission_ov_icon_3')) { ?>



      <section class="section graphic-block-section button-block-media-section-one">
          <div class="wrap">

                <div class="graphic-block">
                    <?php
                        $admission_ov_icon_1 = get_field('admission_ov_icon_1');
                        $admission_ov_icon_2 = get_field('admission_ov_icon_2');
                        $admission_ov_icon_3 = get_field('admission_ov_icon_3');
                        $admission_ov_text_1 = get_field('admission_ov_text_1');
                        $admission_ov_text_2 = get_field('admission_ov_text_2');
                        $admission_ov_text_3 = get_field('admission_ov_text_3');
                      ?>

                    <div class="graphic-block-content pitt-line">
                      <img src="<?php echo $admission_ov_icon_1; ?>" alt="">
                      <h3><?php echo $admission_ov_text_1; ?></h3>
                    </div>

                    <div class="graphic-block-content pitt-line">
                      <img src="<?php echo $admission_ov_icon_2; ?>" alt="">
                      <h3><?php echo $admission_ov_text_2; ?></h3>
                    </div>

                    <div class="graphic-block-content pitt-line">
                      <img src="<?php echo $admission_ov_icon_3; ?>" alt="">
                      <h3><?php echo $admission_ov_text_3; ?></h3>
                    </div>
                </div>

            </div>
        </section>
        <!-- end pitt graphic block -->

        <?php } ?>


<?php get_footer(); ?>
