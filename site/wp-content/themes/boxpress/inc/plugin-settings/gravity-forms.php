<?php
/**
 * Gravity Forms Settings
 */
if ( function_exists( 'gravity_form' )) {

  // Move GF scripts to footer
  add_filter( 'gform_init_scripts_footer', '__return_true' );

  // Scroll to confirmation
  add_filter( 'gform_confirmation_anchor', '__return_true' );
}
