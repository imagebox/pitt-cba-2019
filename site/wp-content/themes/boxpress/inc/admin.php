<?php

/**
 * BoxPress Logo
 */

function my_login_logo() { ?>
  <style type="text/css">
    body.login div#login h1 a {
      background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/branding/boxpress.png);
      padding-bottom: 30px;
      height: 40px;
      width: 140px;
      background-size: 140px, 40px;
    }
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );



/**
 * Show the kitchen sink by default
 */

function unhide_kitchensink( $args ) {
  $args['wordpress_adv_hidden'] = false;
    return $args;
}
add_filter( 'tiny_mce_before_init', 'unhide_kitchensink' );



/**
 * Set default media link to 'none'
 */

update_option('image_default_link_type','none');



/**
 * Set default gallery link to 'file'
 */

function my_gallery_default_type_set_link( $settings ) {
  $settings['galleryDefaults']['link'] = 'file';
  return $settings;
}
add_filter( 'media_view_settings', 'my_gallery_default_type_set_link');




/**
 * Remove 'Editor' from Appearance menu
 */
function remove_editor_menu() {
  remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);

/**
 * Remove CSS section from Appearance menu
 * @param $wp_customize WP_Customize_Manager
 */
function prefix_remove_css_section( $wp_customize ) {
  $wp_customize->remove_section( 'custom_css' );
}
add_action( 'customize_register', 'prefix_remove_css_section', 15 );
