<?php

/*
 * Navigation
 */

register_nav_menus( array(
  'primary'    => __( 'Primary Menu', 'boxpress' ),
  'secondary'  => __( 'Secondary Menu', 'boxpress' ),
  'topfull'  => __( 'Top Full', 'boxpress' ),
  'topone'  => __( 'Top Menu One', 'boxpress' ),
  'toptwo'  => __( 'Top Menu Two', 'boxpress' ),
  'footerone'  => __( 'Footer Menu One', 'boxpress' ),
  'footertwo'  => __( 'Footer Menu Two', 'boxpress' ),
  'button'     => __( 'Button Menu', 'boxpress' ),
));
