<?php
/**
 * Global Taxonomies
 */

/**
 * Featured Post Tax
 */

function tax_featured_post() {

  $label_plural   = 'Featured Pages';
  $label_singular = 'Featured Page';

  $labels = array(
    'name'                       => _x( "{$label_plural}", 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( "{$label_singular}", 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( "{$label_plural}", 'boxpress' ),
    'all_items'                  => __( "All {$label_plural}", 'boxpress' ),
    'parent_item'                => __( "Parent {$label_singular}", 'boxpress' ),
    'parent_item_colon'          => __( "Parent {$label_singular}:", 'boxpress' ),
    'new_item_name'              => __( "New {$label_singular} Name", 'boxpress' ),
    'add_new_item'               => __( "Add New {$label_singular}", 'boxpress' ),
    'edit_item'                  => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'                => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'                  => __( "View {$label_singular}", 'boxpress' ),
    'separate_items_with_commas' => __( "Separate {$label_plural} with commas", 'boxpress' ),
    'add_or_remove_items'        => __( "Add or remove {$label_plural}", 'boxpress' ),
    'choose_from_most_used'      => __( "Choose from the most used", 'boxpress' ),
    'popular_items'              => __( "Popular {$label_plural}", 'boxpress' ),
    'search_items'               => __( "Search {$label_plural}", 'boxpress' ),
    'not_found'                  => __( "Not Found", 'boxpress' ),
    'items_list'                 => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation'      => __( "{$label_plural} list navigation", 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite' => false,
  );

  register_taxonomy( 'featured_post', 'post', $args );
}
add_action( 'init', 'tax_featured_post' );



/**
 * Media Tax
 */

// Style Tax
function tax_media_style() {

  $label_plural   = 'Styles';
  $label_singular = 'Style';

  $labels = array(
    'name'                       => _x( "{$label_plural}", 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( "{$label_singular}", 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( "{$label_plural}", 'boxpress' ),
    'all_items'                  => __( "All {$label_plural}", 'boxpress' ),
    'parent_item'                => __( "Parent {$label_singular}", 'boxpress' ),
    'parent_item_colon'          => __( "Parent {$label_singular}:", 'boxpress' ),
    'new_item_name'              => __( "New {$label_singular} Name", 'boxpress' ),
    'add_new_item'               => __( "Add New {$label_singular}", 'boxpress' ),
    'edit_item'                  => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'                => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'                  => __( "View {$label_singular}", 'boxpress' ),
    'separate_items_with_commas' => __( "Separate {$label_plural} with commas", 'boxpress' ),
    'add_or_remove_items'        => __( "Add or remove {$label_plural}", 'boxpress' ),
    'choose_from_most_used'      => __( "Choose from the most used", 'boxpress' ),
    'popular_items'              => __( "Popular {$label_plural}", 'boxpress' ),
    'search_items'               => __( "Search {$label_plural}", 'boxpress' ),
    'not_found'                  => __( "Not Found", 'boxpress' ),
    'items_list'                 => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation'      => __( "{$label_plural} list navigation", 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite' => false,
  );

  register_taxonomy( 'media_style', 'attachment', $args );
}
add_action( 'init', 'tax_media_style' );

// Color Tax
function tax_media_color() {

  $label_plural   = 'Colors';
  $label_singular = 'Color';

  $labels = array(
    'name'                       => _x( "{$label_plural}", 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( "{$label_singular}", 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( "{$label_plural}", 'boxpress' ),
    'all_items'                  => __( "All {$label_plural}", 'boxpress' ),
    'parent_item'                => __( "Parent {$label_singular}", 'boxpress' ),
    'parent_item_colon'          => __( "Parent {$label_singular}:", 'boxpress' ),
    'new_item_name'              => __( "New {$label_singular} Name", 'boxpress' ),
    'add_new_item'               => __( "Add New {$label_singular}", 'boxpress' ),
    'edit_item'                  => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'                => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'                  => __( "View {$label_singular}", 'boxpress' ),
    'separate_items_with_commas' => __( "Separate {$label_plural} with commas", 'boxpress' ),
    'add_or_remove_items'        => __( "Add or remove {$label_plural}", 'boxpress' ),
    'choose_from_most_used'      => __( "Choose from the most used", 'boxpress' ),
    'popular_items'              => __( "Popular {$label_plural}", 'boxpress' ),
    'search_items'               => __( "Search {$label_plural}", 'boxpress' ),
    'not_found'                  => __( "Not Found", 'boxpress' ),
    'items_list'                 => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation'      => __( "{$label_plural} list navigation", 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite' => false,
  );

  register_taxonomy( 'media_color', 'attachment', $args );
}
add_action( 'init', 'tax_media_color' );

// Details Tax
function tax_media_details() {

  $label_plural   = 'Details';
  $label_singular = 'Details';

  $labels = array(
    'name'                       => _x( "{$label_plural}", 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( "{$label_singular}", 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( "{$label_plural}", 'boxpress' ),
    'all_items'                  => __( "All {$label_plural}", 'boxpress' ),
    'parent_item'                => __( "Parent {$label_singular}", 'boxpress' ),
    'parent_item_colon'          => __( "Parent {$label_singular}:", 'boxpress' ),
    'new_item_name'              => __( "New {$label_singular} Name", 'boxpress' ),
    'add_new_item'               => __( "Add New {$label_singular}", 'boxpress' ),
    'edit_item'                  => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'                => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'                  => __( "View {$label_singular}", 'boxpress' ),
    'separate_items_with_commas' => __( "Separate {$label_plural} with commas", 'boxpress' ),
    'add_or_remove_items'        => __( "Add or remove {$label_plural}", 'boxpress' ),
    'choose_from_most_used'      => __( "Choose from the most used", 'boxpress' ),
    'popular_items'              => __( "Popular {$label_plural}", 'boxpress' ),
    'search_items'               => __( "Search {$label_plural}", 'boxpress' ),
    'not_found'                  => __( "Not Found", 'boxpress' ),
    'items_list'                 => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation'      => __( "{$label_plural} list navigation", 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite' => false,
  );

  register_taxonomy( 'media_details', 'attachment', $args );
}
add_action( 'init', 'tax_media_details' );


// Include in Inspiration
function tax_media_inspiration() {

  $label_plural   = 'Is Inspiration';
  $label_singular = 'Is Inspiration';

  $labels = array(
    'name'                       => _x( "{$label_plural}", 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( "{$label_singular}", 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( "{$label_plural}", 'boxpress' ),
    'all_items'                  => __( "All {$label_plural}", 'boxpress' ),
    'parent_item'                => __( "Parent {$label_singular}", 'boxpress' ),
    'parent_item_colon'          => __( "Parent {$label_singular}:", 'boxpress' ),
    'new_item_name'              => __( "New {$label_singular} Name", 'boxpress' ),
    'add_new_item'               => __( "Add New {$label_singular}", 'boxpress' ),
    'edit_item'                  => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'                => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'                  => __( "View {$label_singular}", 'boxpress' ),
    'separate_items_with_commas' => __( "Separate {$label_plural} with commas", 'boxpress' ),
    'add_or_remove_items'        => __( "Add or remove {$label_plural}", 'boxpress' ),
    'choose_from_most_used'      => __( "Choose from the most used", 'boxpress' ),
    'popular_items'              => __( "Popular {$label_plural}", 'boxpress' ),
    'search_items'               => __( "Search {$label_plural}", 'boxpress' ),
    'not_found'                  => __( "Not Found", 'boxpress' ),
    'items_list'                 => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation'      => __( "{$label_plural} list navigation", 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite' => false,
  );

  register_taxonomy( 'media_inspiration', 'attachment', $args );
}
add_action( 'init', 'tax_media_inspiration' );


/**
 * Search & Filter Query Vars
 * ---
 * Used in the Archive template for search and filter functionality
 */

function boxoress_media_query_vars( $vars ) {
  $vars[] = 'filter_style';
  $vars[] = 'filter_color';
  $vars[] = 'filter_details';
  return $vars;
}
add_filter( 'query_vars', 'boxoress_media_query_vars' );
