<?php 

/**
 * BoxPress Locations Custom Post Type
 *
 * @package BoxPress
 */


/**
 * Register Custom Post Type
 */

function cpt_locations() {

  $labels = array(
    'name'                  => _x( 'Locations', 'Post Type General Name', 'boxpress' ),
    'singular_name'         => _x( 'Location', 'Post Type Singular Name', 'boxpress' ),
    'menu_name'             => __( 'Locations', 'boxpress' ),
    'name_admin_bar'        => __( 'Location', 'boxpress' ),
    'parent_item_colon'     => __( 'Parent Location:', 'boxpress' ),
    'all_items'             => __( 'All Locations', 'boxpress' ),
    'add_new_item'          => __( 'Add New Location', 'boxpress' ),
    'add_new'               => __( 'Add New', 'boxpress' ),
    'new_item'              => __( 'New Location', 'boxpress' ),
    'edit_item'             => __( 'Edit Location', 'boxpress' ),
    'update_item'           => __( 'Update Location', 'boxpress' ),
    'view_item'             => __( 'View Location', 'boxpress' ),
    'search_items'          => __( 'Search Location', 'boxpress' ),
    'not_found'             => __( 'Not found', 'boxpress' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'boxpress' ),
    'items_list'            => __( 'Locations list', 'boxpress' ),
    'items_list_navigation' => __( 'Locations list navigation', 'boxpress' ),
    'filter_items_list'     => __( 'Filter locations list', 'boxpress' ),
  );
  $args = array(
    'label'                 => __( 'Location', 'boxpress' ),
    'description'           => __( 'Location Custom Post Type with Map', 'boxpress' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
    'taxonomies'            => array( 'location_categories' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,    
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'rewrite' => array(
      'with_front' => false,
    ),
  );
  register_post_type( 'locations', $args );

}
add_action( 'init', 'cpt_locations', 0 );



/**
 * Register Custom Taxonomy
 */
function taxonomy_location_categories() {

  $labels = array(
    'name'                       => _x( 'Categories', 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( 'Categories', 'boxpress' ),
    'all_items'                  => __( 'All Categories', 'boxpress' ),
    'parent_item'                => __( 'Parent Category', 'boxpress' ),
    'parent_item_colon'          => __( 'Parent Category:', 'boxpress' ),
    'new_item_name'              => __( 'New Category Name', 'boxpress' ),
    'add_new_item'               => __( 'Add New Category', 'boxpress' ),
    'edit_item'                  => __( 'Edit Category', 'boxpress' ),
    'update_item'                => __( 'Update Category', 'boxpress' ),
    'view_item'                  => __( 'View Category', 'boxpress' ),
    'separate_items_with_commas' => __( 'Separate categories with commas', 'boxpress' ),
    'add_or_remove_items'        => __( 'Add or remove categories', 'boxpress' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'boxpress' ),
    'popular_items'              => __( 'Popular Categories', 'boxpress' ),
    'search_items'               => __( 'Search Categories', 'boxpress' ),
    'not_found'                  => __( 'Not Found', 'boxpress' ),
    'items_list'                 => __( 'Categories list', 'boxpress' ),
    'items_list_navigation'      => __( 'Categories list navigation', 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
  );
  register_taxonomy( 'location_categories', array( 'locations' ), $args );

}
add_action( 'init', 'taxonomy_location_categories', 0 );
