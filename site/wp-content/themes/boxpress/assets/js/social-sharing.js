(function ($) {
  'use strict';

  function windowPopup( url, width, height ) {
    // Calculate the position of the popup so
    // it’s centered on the screen.
    var pos_left  = (screen.width / 2) - (width / 2);
    var pos_top   = (screen.height / 2) - (height / 2);
    var popup_url = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=' + width + ',height=' + height + ',top=' + pos_top + ',left=' + pos_left;

    window.open( url, '', popup_url );
  }
  
  $('.js-social-share').on('click', function () {
    var this_href = $(this).attr('href');

    windowPopup( this_href, 500, 300 );

    return false;
  });
  
})(jQuery);
