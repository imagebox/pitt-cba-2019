(function ($) {
  'use strict';

  /**
   * Site javascripts
   */

  // Wrap iframes in div w/ flexible-container class to make responsive
  $("iframe:not(.not-responsive)").wrap('<div class="flexible-container"></div>');

  var $owl_carousel = $('.js-owl-carousel');
  $owl_carousel.each(function () {
    var $this = $(this);
    var carousel_loop = false;
    var carousel_nav  = true;
    var carousel_dots = false;
    var mouse_drag    = false;
    var touch_drag    = false;

    if ( $this.find( '> div' ).length > 1 ) {
      carousel_loop = true;
      carousel_nav  = true;
      carousel_dots = true;
      mouse_drag    = true;
      touch_drag    = true;
    }

    // Carousels
    $this.owlCarousel({
      items: 1,
      mouseDrag: mouse_drag,
      touchDrag: touch_drag,
      dots: carousel_dots,
      nav: carousel_nav,
      loop: carousel_loop,
      navSpeed: 500,
      dotsSpeed: 500,
      dragEndSpeed: 500,
      autoplayHoverPause: true,
      // autoWidth: true,
      thumbs: true,
    thumbsPrerendered: true,
      navText: [
        '<svg class="svg-left-icon" width="16" height="19"><use xlink:href="#arrow-left-icon"></use></svg>',
        '<svg class="svg-right-icon" width="16" height="19"><use xlink:href="#arrow-right-icon"></use></svg>',
      ]
    });
  });
  /* make mobile menu sub menus work as accordians
	$(".navigation--mobile ul > li.menu-item-has-children").children('a').toggle(
		function(){$(this).parent('li').children('.sub-menu').slideDown( "slow" );},
		function(){$(this).parent('li').children('.sub-menu').slideUp( "slow" );
	});


  $('.mobile-main-nav li.menu-item-has-children a').click(function() {
    $(this).toggleClass('open');
  });
*/



/**
  * Popups
  */

 $('.js-image-gallery-popup').magnificPopup({
   type: 'image',
   gallery: {
     enabled: true,
   },
   image: {
     titleSrc: 'data-caption',
   }
 });



 $('.news-mobile-close-button').click(function(){
   $('.cat-nav').toggleClass('cat-open');
   var icon = $(this).find(".mobile-cat-close");
   icon.toggleClass("mobile-cat-open")
 })


  $('h2').filter(function() {
      return $(this).attr('style') && $(this).attr('style').indexOf('text-align: center;') >= 0;
  }).show().addClass('center-line');

  	// Video(s)
  	$('.video-button').magnificPopup({
  		type: 'iframe'
  	});

    $('.tabs').each(function() {

    });

    $('.tabs .tab:first-child').addClass('current');
    $('.tab-contents .tab-content:first-child').addClass('current');

    $('.tabs button.tab').click(function(){

        var tab_id = $(this).attr('data-tab');

    		$('.tabs button.tab').removeClass('current');
    		$('.tab-content').removeClass('current');

    		$(this).addClass('current');
    		$("#tab-"+tab_id).addClass('current');

    });
    // tabs

    /**
     * Pinterist Share Link
     */

    $('.content--single img[class*="wp-image"], .post-slide-image').each(function () {
      var $this   = $(this);
      var src_url = $this.attr('src');
      var img_width = $this.attr('width');
      var href    = 'https://www.pinterest.com/pin/create/bookmarklet/?url=' + escape( window.location.href ) + '&media=' + escape( src_url ) + '&description=' + escape( $this.attr('alt') ) + '&is_video=false';

      if ( $this.parent().is('p') ) {
        $this.unwrap();
      }
      $this.wrap('<div class="post-pin-link-wrap"></div>');
      $this.parent().append('<a class="post-pin-link" href="' + href + '"><svg width="20" height="20"><use xlink:href="#social-pinterest"></svg></a>');

      if ( img_width !== '' ) {
        $this.parent().css( 'max-width', img_width + 'px' );
      }
    });

})(jQuery);

var accordions = document.getElementsByClassName('accordion');


for (var i = 0; i < accordions.length; i++){
  accordions[i].onclick = function () {
    this.classList.toggle('accord-is-open');
    var content = this.nextElementSibling;

    if (content.style.maxHeight) {
        content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  };
}

// end accordions //
