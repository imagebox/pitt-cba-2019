(function ($) {
  'use strict';

  /**
   * WP Aria Navigations
   * ---
   * Use in conjunction with WP Aria Menu Walker for a
   * more accessible navigation. Heavily uses timers to
   * gage mouse intent.
   * 
   * Features:
   * - Toggle 'aria-expanded' attribute on focus and hover
   * - Display sub-menu on tab focus
   * - Fixed Sub-Menu - Sub-menu displays fixed on 'moutseout' or
   *   until user clicks out
   * - Hover Intent like feature - Don't enable the fixed sub-menu
   *   until mouse stays on menu item for extended time
   *
   * Todo:
   * - Inline CSS
   * - Add option for disabling overlay
   * - Only display overlay if mouse delay is being used or overlay is specificly enabled
   */

  $.fn.wpAriaNav = function ( options ) {

    options = jQuery.extend({
      overlayClass    : 'js-aria-hover-overlay',
      menuHoverClass  : 'is-active',
      mouseOutTimeout : 0,
      mouseDelay      : 350,
    }, options );

    var $aria_nav       = $(this);
    var $nav_top_item   = $aria_nav.find('> ul > li > a');
    var $aria_dropdown  = $aria_nav.find('li[aria-haspopup="true"]');

    var unset_active_delay = 0;
    var mouseleave_timer;
    var delay_timer;
    var first_leave_timer;
    var is_first_leave = false;

    // Add blank overlay
    $aria_nav.after('<div class="' + options.overlayClass + '"></div>');
    var $aria_nav_overlay = $aria_nav.next( '.' + options.overlayClass );


    // Unset all active Aria items and activate the current
    $aria_dropdown.on('focusin mouseenter', function () {
      var $this = $(this);
      var mouse_delay = options.mouseDelay;

      // Unset active if another item is hovered
      aria_unset_active( $aria_dropdown );

      // Reset our timeout when mouse re-enters after
      // triggering a mouseleave
      clearTimeout( mouseleave_timer );

      // Enable active state
      $this.attr( 'aria-expanded', 'true' )
        .addClass( options.menuHoverClass );

      $aria_nav_overlay.addClass( options.menuHoverClass );


      // Prevent immediate close if we re-enter the menu quickly
      if ( is_first_leave === true ) {
        mouse_delay = 0;
      }


      /**
       * Ignore mouse if it's 'just passing through'
       * ---
       * `unset_active_delay` default is 0 and is only updated if
       * the mouse is sticking around
       */

      delay_timer = setTimeout(function () {
        unset_active_delay = options.mouseOutTimeout;
      }, mouse_delay );

    });


    // Unset all active items
    $nav_top_item.on('focusin mouseenter', function () {
      aria_unset_active( $aria_dropdown );
    });


    // Don't close dropdowns right away when mouse leaves
    $aria_dropdown.on('mouseleave', function () {

      /**
       * Delay sub-nav closing
       */

      mouseleave_timer = setTimeout(function () {
        aria_unset_active( $aria_dropdown );
      }, unset_active_delay );


      /**
       * Reset 'Just passing through' Timer
       */

      unset_active_delay = 0;
      clearTimeout( delay_timer );


      /**
       * Track if mouse re-enters quickly
       * ---
       * Bypasses the 'Just passing through' timer to allow
       * sub-nav to stick around if mouse accidentally leaves
       * and comes back quickly
       */

      is_first_leave = true;

      clearTimeout( first_leave_timer );
      first_leave_timer = setTimeout(function () {
        is_first_leave = false;
      }, options.mouseDelay );

    });


    // Unset active when overlay is clicked
    $aria_nav_overlay.on('click', function () {
      var $this = $(this);
      aria_unset_active( $aria_dropdown );
    });


    // Change Aria status and remove active class
    function aria_unset_active( $aria_dropdown ) {
      $aria_dropdown.attr( 'aria-expanded', 'false' )
        .removeClass( options.menuHoverClass );
      $aria_nav_overlay.removeClass( options.menuHoverClass );
    }
  };

})(jQuery);
