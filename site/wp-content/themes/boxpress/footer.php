
</main>

<?php if ( have_rows( 'footer_button_block' )) : ?>
  <?php while ( have_rows( 'footer_button_block' )) : the_row();
      $footer_callout_copy = get_sub_field('footer_callout_copy');
      $footer_callout_title = get_sub_field('heading');
      $footer_button_link = get_sub_field('button_link');
      $background = get_sub_field('background');
      $display_button_block = get_sub_field('display_button_block');
    ?>

    <section class="section why-button-block footer-button-block desktop <?php echo $background; ?> <?php echo $display_button_block; ?>">

      <div class="wrap wrap--limited">
        <div class="button-block">

          <?php if ( ! empty( $footer_callout_title )) : ?>

            <div class="callout-header">
              <h2><?php echo $footer_callout_title; ?></h2>
            </div>

          <?php endif; ?>

          <div class="callout-body">
            <div class="button-callout-content">
              <?php if ( ! empty( $footer_callout_copy )) : ?>
                <p><?php echo $footer_callout_copy; ?></p>
              <?php endif; ?>

              <?php if ( $footer_button_link ) : ?>
                  <a class="button"
                  href="<?php echo esc_url( $footer_button_link['url'] ); ?>"
                  target="<?php echo $footer_button_link['target']; ?>">
                  <?php echo $footer_button_link['title']; ?>
                </a>
              <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </section>

  <?php endwhile; ?>
<?php endif; ?>

<!-- Footer Button Block  -->

<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="primary-footer">
      <h6><?php the_field('footer_heading', 'option'); ?></h6>
    <div class="wrap">
      <div class="footer-col-1 pitt-line">
        <address class="footer-address">
          <h6>College of Business Administration</h6>
          <?php get_template_part( 'template-parts/address-block' ); ?>
            <h6><?php the_field('footer_heading', 'option'); ?></h6>
        </address>
      </div>

      <div class="footer-col-2 pitt-line">
          <nav class="js-accessible-menu navigation--main"
            aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
            role="navigation">
            <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'footerone',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
            </ul>

            <img src="<?php bloginfo('template_directory'); ?>/assets/img/footer-logo-large.png" />

          </nav>
      </div>

      <div class="footer-col-3">
        <nav class="js-accessible-menu navigation--main"
          aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
          role="navigation">
          <ul class="nav-list">
              <?php
                wp_nav_menu( array(
                  'theme_location'  => 'footertwo',
                  'items_wrap'      => '%3$s',
                  'container'       => false,
                  'walker'          => new Aria_Walker_Nav_Menu(),
                ));
              ?>
              <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
               <label>
                 <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
                 <input type="search" class="search-field"
                 placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>"
                 value="<?php echo get_search_query() ?>" name="s"
                 title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
               </label>
               <input type="submit" class="search-submit"
               value="" />
         </form>
          </ul>
        </nav>
      </div>

      <div class="footer-col-4">
        <div class="footer-social">
          <?php get_template_part( 'template-parts/social-nav' ); ?>
          <svg class="site-logo" width="136" height="136">
              <use xlink:href="#logo-seal"></use>
          </svg>
        </div>
      </div>

    </div>
  </div>

</footer>

<?php wp_footer(); ?>

<?php // GA Tracking Code ?>
<?php the_field( 'tracking_code', 'option' ); ?>

</body>
</html>
