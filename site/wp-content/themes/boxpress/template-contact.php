<?php
/**
 * Template Name: Contact
 */


$office_name_1 = get_field('office_name_1');
$office_addy_1 = get_field('office_addy_1');
$office_city_1 = get_field('office_local_1');
$office_phone_1 = get_field('office_phone_1');
$office_fax_1 = get_field('office_fax_1');
$office_email_1 = get_field('office_email_1');


$office_name_2 = get_field('office_name_2');
$office_addy_2 = get_field('office_addy_2');
$office_city_2 = get_field('office_local_2');
$office_phone_2 = get_field('office_phone_2');
$office_fax_2 = get_field('office_fax_2');
$office_email_2 = get_field('office_email_2');


$office_name_3 = get_field('office_name_3');
$office_addy_3 = get_field('office_addy_3');
$office_city_3 = get_field('office_local_3');
$office_phone_3 = get_field('office_phone_3');
$office_fax_3 = get_field('office_fax_3');
$office_email_3 = get_field('office_email_3');

$office_3_title = get_field('office_3_title');
$office_3_service_1 = get_field('office_3_service_1');
$office_3_service_2 = get_field('office_3_service_2');
$office_3_service_3 = get_field('office_3_service_3');
$office_3_service_4 = get_field('office_3_service_4');
$office_3_service_5 = get_field('office_3_service_5');



get_header(); ?>


  <article class="homepage">

    <?php get_template_part('template-parts/banners/banner--contact'); ?>

    <section class="fullwidth-column advanced-full-width section <?php echo $background; ?>">
      <div class="wrap">

        <div class="l-sidebar">
          <div class="l-main contact-offices contact-main">

              <h3>Offices</h3>
              <div class="office-box">
                <div class="office-box-1">
                  <ul>
                    <p class="office-title"><?php echo $office_name_1 ?></p>
                    <li><?php echo $office_addy_1 ?></li>
                    <li><?php echo $office_city_1 ?></li>
                    <li><?php echo $office_phone_1 ?></li>
                    <li><?php echo $office_fax_1 ?></li>
                    <li> <a class="no-underline" href="#"><?php echo $office_email_1 ?></a></li>
                  </ul>
                  <ul>
                    <p class="office-title"><?php echo $office_name_2 ?></p>
                    <li><?php echo $office_addy_2 ?></li>
                    <li><?php echo $office_city_2 ?></li>
                    <li><?php echo $office_phone_2 ?></li>
                    <li><?php echo $office_fax_2 ?></li>
                    <li> <a class="no-underline" href="#"><?php echo $office_email_2 ?></a></li>
                  </ul>
                </div>

              </div>
              <div class="office-box-2">
                <ul>
                  <p class="office-title"><?php echo $office_name_3 ?></p>
                  <li><?php echo $office_addy_3 ?></li>
                  <li><?php echo $office_city_3 ?></li>
                  <li><?php echo $office_phone_3 ?></li>
                  <li><?php echo $office_fax_3 ?></li>
                  <li> <a class="no-underline" href="#"><?php echo $office_email_3 ?></a></li>
                </ul>
                <ul class="w-bullets">
                  <p><?php echo $office_3_title ?></p>
                  <li><?php echo $office_3_service_1 ?></li>
                  <li><?php echo $$office_3_service_2 ?></li>
                  <li><?php echo $office_3_service_3 ?></li>
                  <li><?php echo $office_3_service_4 ?></li>
                  <li><?php echo $office_3_service_5 ?></li>
                </ul>
              </div>
          </div>

            <div class="l-aside">
              <?php get_sidebar(); ?>
            </div>
        </div>

      </div>
    </section>


<!-- office  -->


    <?php
      $staff_location_terms_args = array(
        'taxonomy' => 'staff_location',
      );
      $staff_location_terms = get_terms( $staff_location_terms_args );
    ?>


    <?php if ( $staff_location_terms ) : ?>
      <?php foreach ( $staff_location_terms as $term ) :
            $term_slug = $term->slug;
            $term_name = $term->name;
          ?>

          <?php
            $staff_query_args = array(
              'post_type' => 'staff',
              'posts_per_page' => -1,
              'tax_query' => array(
            		array(
            			'taxonomy' => 'staff_location',
            			'field'    => 'slug',
            			'terms'    => $term_slug,
            		),
            	),
            );
            $staff_query = new $wp_query( $staff_query_args );
          ?>

          <?php if ( $staff_query->have_posts() ) : ?>
            <section class="section contact-section contact-associate-dean-block-photos">
              <div class="wrap">
                <h1 class="contact-staff-title">Staff</h1>
                  <h2><?php echo $term_name; ?></h2>
                <div class="l-grid l-grid--three-col">
                  <?php while ( $staff_query->have_posts() ) : $staff_query->the_post(); ?>
                    <div class="l-grid-item">
                      <?php get_template_part( 'template-parts/staff-member' ); ?>
                    </div>
                  <?php endwhile; ?>
                </div>
              </div>
            </section>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>
<!-- end office  -->

  </article>
<?php get_footer(); ?>
