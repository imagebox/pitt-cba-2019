<?php
/**
 * Template Name: Advanced (Sidebar)
 */
get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <div class="pitt-advanced-width">

      <div class="wrap">

        <div class="content-area">
            <?php
              /**
               * Run clean shortcode for repeater sub-fields.
               *
               * Needs to be calld directly before repeaters, won't
               * work directly in functions.php because of how
               * sub-fields are called.
               */
              function boxpress_clean_shortcodes_acf( $content ) {

                // els to remove
                $array = array(
                  '<p>['    => '[',
                  ']</p>'   => ']',
                  '<div>['  => '[',
                  ']</div>' => ']',
                  ']<br />' => ']',
                  ']<br>'   => ']',
                  '<br />[' => '[',
                  '<br>['   => '[',
                );

                // Remove dem els
                $content = strtr( $content, $array );
                return $content;
              }
              add_filter('acf_the_content', 'boxpress_clean_shortcodes_acf');
            ?>

            <?php if ( have_rows( 'innerpage_master' )) :
                $row_index = 1;
              ?>

              <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <?php while ( have_rows( 'innerpage_master' )) : the_row(); ?>

                  <?php
                    // Create layout string
                    $layout_path = 'template-layouts-sidebar/' . str_replace( '_', '-', get_row_layout()) . '.php';

                    // Include our layout
                    if (( @include $layout_path ) === false ) {
                      echo '<div class="boxpress-error"><p>Sorry bud, that layout file is missing!</p></div>';
                    }

                    $row_index++;
                  ?>

                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>

              </article>

            <?php endif; ?>

            <?php if ( have_rows( 'footer_button_block' )) : ?>
              <?php while ( have_rows( 'footer_button_block' )) : the_row();
                  $footer_callout_copy = get_sub_field('footer_callout_copy');
                  $footer_callout_title = get_sub_field('heading');
                  $footer_button_link = get_sub_field('button_link');
                  $background = get_sub_field('background');
                  $display_button_block = get_sub_field('display_button_block');
                ?>

                <section class="section why-button-block footer-button-block mobile <?php echo $background; ?> <?php echo $display_button_block; ?>">

                  <div class="wrap wrap--limited">
                    <div class="button-block">

                      <?php if ( ! empty( $footer_callout_title )) : ?>

                        <div class="callout-header">
                          <h2><?php echo $footer_callout_title; ?></h2>
                        </div>

                      <?php endif; ?>

                      <div class="callout-body">
                        <div class="button-callout-content">
                          <?php if ( ! empty( $footer_callout_copy )) : ?>
                            <p><?php echo $footer_callout_copy; ?></p>
                          <?php endif; ?>

                          <?php if ( $footer_button_link ) : ?>
                              <a class="button"
                              href="<?php echo esc_url( $footer_button_link['url'] ); ?>"
                              target="<?php echo $footer_button_link['target']; ?>">
                              <?php echo $footer_button_link['title']; ?>
                            </a>
                          <?php endif; ?>

                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <?php endwhile; ?>
            <?php endif; ?>

        </div><!--.content-area-->


        <div class="pitt-sidebar">
            <?php require('sidebar.php'); ?>
        </div>

      </div>

  </div><!--.some-class-name-->


<?php get_footer(); ?>
